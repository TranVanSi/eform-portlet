<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://java.sun.com/portlet" prefix="portlet" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>
<%@ taglib uri="http://liferay.com/tld/portlet" prefix="liferay-portlet" %>
<%@ include file="../init.jsp" %>
<script src="${pageContext.request.contextPath}/js/cauhinheform.js"></script>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/css/cauhinheform.css">
</head>
<portlet:renderURL var="inURL">
    <portlet:param name="action" value="in"/>
</portlet:renderURL>
<portlet:renderURL var="themMoiURL">
    <portlet:param name="action" value="themMoiIn"/>
</portlet:renderURL>
<liferay-portlet:actionURL var="xoaUrl">
    <portlet:param name="action" value="xoaIn"></portlet:param>
</liferay-portlet:actionURL>
<liferay-portlet:resourceURL var="getDanhSachTemplateURL" id="getDanhSachTemplate"></liferay-portlet:resourceURL>
<liferay-ui:success key="success" message="Yêu cầu thực hiện thành công!"/>
<liferay-ui:success key="form-success" message="${thongBao}"/>

<input type="hidden" id="xoaUrl" value="${xoaUrl}" />
<input type="hidden" id="themMoiURL" value="${themMoiURL}" />
<input type="hidden" id="maForm" value="${maForm}" />
<input type="hidden" id="getDanhSachTemplateURL" value="${getDanhSachTemplateURL}" />
<main class="main-table-custom">
    <div class="col-md-12 col-sm-12 col-xs-12 center">
        <label class="tieuDe">DANH SÁCH TEMPLATE</label>
    </div>
    <div class="file-button">
        <button style="float: right" class="btn btn-primary" onclick="location.href='${themMoiURL}&maForm=${maForm}'">Thêm mới</button>
    </div>
    <table class="table table-bordered" id="TEMPLATE_PRINT"
           data-pagination="true" data-toggle="table" data-side-pagination="server" data-mobile-responsive="true"
           data-ajax="ajaxRequest" data-id-field="_id">
        <thead class="thead-light">
        <tr>
            <th data-formatter="formatterRowIndex" data-field="stt" class="stt">STT</th>
            <th data-field="tenForm" class="tenForm">tên</th>
            <th data-formatter="formatterButtons" data-field="thaoTac" class="thaoTac">Thao tác</th>
        </tr>
        </thead>
        <tbody id="bodyTEMPLATE_PRINT"></tbody>
    </table>
</main>

<script src="${pageContext.request.contextPath}/js/danhsachtemplate.js"></script>