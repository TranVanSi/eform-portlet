package vn.sdt.eform.crudform.dto;

import lombok.Data;

@Data
public class DuLieuDTO {
    private String id;
    private String duLieu;
}
