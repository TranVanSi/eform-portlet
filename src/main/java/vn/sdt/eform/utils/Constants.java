package vn.sdt.eform.utils;

public class Constants {
    public static interface RESPONE {
        public static final int SUCCESS = 1;
    }
    public static final String DEFAULT_FINAL = "DEFAULT_FINAL";
    public static final String MA_FORM_TEMPLATE_PRINT = "TEMPLATE_PRINT";

    public static final String SLASH = "/";

    public static interface LOAI_FORM {
        public static final int THEM_MOI = 1;
        public static final int DANH_SACH = 2;
        public static final int CHI_TIET = 3;
        public static final int CREATE_SERVICE = 4;
        public static final int DANHSACH_SERVICE = 5;
    }

    public static final String UPLOAD_URL = "http://localhost:9020";

    public static final String TEN_FORM_DEFAULT = "FORM NAME";

    public static interface METHOD {
        public static final String POST = "POST";
        public static final String GET = "GET";
        public static final String DELETE = "DELETE";
        public static final String PUT = "PUT";
    }

    public static interface AUTHEN {
        public static final String BEARER = "bearer";
        public static final String BASIC = "basic";

    }

    public static interface CREATE_COLLECTION_TABLE {
        public static final String STT = "%22stt%22,";
        public static final String ENCODE_DAU_NHAY = "%22";
        public static final String PATH_PHAN_TRANG = "?offset=0&limit=10";
    }

    public static interface LOAI_AUTHEN {
        public static final int BEARER = 1;
        public static final int BASIC = 2;

    }

    public static interface CHECK_DATA {
        public static final int JSON_OBJECT = 1;
        public static final int JSON_ARRAY = 2;
        public static final int STRING = 3;
    }

    public static final String ERROR_AUTHEN = "401 Unauthorized";
}
