package vn.sdt.eform.crudform.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UploadFileDTO {
    private Long id;
    private String originFileName;
    private String fileDownloadUri;
    private String fileType;
    private long size;
}
