<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://java.sun.com/portlet" prefix="portlet" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>
<%@ taglib uri="http://liferay.com/tld/portlet" prefix="liferay-portlet" %>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/css/chitietform.css">
</head>

<div id="print-me">
</div>

<liferay-portlet:resourceURL var="getDuLieuTableUrl" id="getDuLieuTable" />
<liferay-portlet:resourceURL var="searchDuLieuFormUrl" id="searchDuLieuForm" />
<liferay-ui:success key="success" message="Yêu cầu thực hiện thành công!"/>
<input type="hidden" id="objResult" value='${obj}' />
<div id="print" style="text-align: left">
    ${obj}
</div>
<div class="text-center w-100">
    <button class="btn btn-info" type="button" onclick="inMau();">In</button>
    <button class="btn btn-info" type="button" onclick="window.history.back(-1);">Quay lại</button>
</div>



<script>
    function inMau() {
        var printContents = document.getElementById('print').innerHTML;
        var originalContents = document.body.innerHTML;

        document.body.innerHTML = printContents;

        window.print();

        document.body.innerHTML = originalContents;
    };

</script>
