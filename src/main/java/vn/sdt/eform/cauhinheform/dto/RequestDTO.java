package vn.sdt.eform.cauhinheform.dto;

import com.fasterxml.jackson.databind.JsonNode;
import lombok.Data;

@Data
public class RequestDTO {
    private String method;
    private JsonNode header;
    private JsonNode auth;
    private JsonNode body;
    private JsonNode url;
    private JsonNode token;
}
