package vn.sdt.eform.crudform.controllers;

import com.liferay.portal.kernel.portlet.DefaultConfigurationAction;
import com.liferay.portal.kernel.portlet.PortletPreferencesFactoryUtil;
import com.liferay.portal.kernel.util.ParamUtil;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;
import vn.sdt.eform.cauhinheform.client.CauHinhEformClient;
import vn.sdt.eform.cauhinheform.dto.CauHinhEformDTO;
import vn.sdt.eform.config.Config;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletConfig;
import javax.portlet.PortletPreferences;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

public class ConfigAction extends DefaultConfigurationAction {
    @Override
    public void processAction(PortletConfig config, ActionRequest request, ActionResponse response) throws Exception {

        String portletResource = ParamUtil.getString(request, "portletResource");

        request.getPortletSession().setAttribute("portletResource", portletResource);

        PortletPreferences prefs = PortletPreferencesFactoryUtil.getPortletSetup(request, portletResource);
        prefs.setValue("maForm", ParamUtil.getString(request, "maForm"));
        prefs.setValue("isImport", ParamUtil.getString(request, "isImport"));
        prefs.store();
    }

    @Override
    public void include(PortletConfig portletConfig, HttpServletRequest httpServletRequest,
                        HttpServletResponse httpServletResponse) throws Exception {

        PortletPreferences pref = PortletPreferencesFactoryUtil.getPortletSetup(
                httpServletRequest,
                ParamUtil.getString(httpServletRequest, "portletResource")
        );

        String maForm = pref.getValue("maForm", "" );
        String isImport = pref.getValue("isImport", "" );

        AbstractApplicationContext appContext = new AnnotationConfigApplicationContext(Config.class, CauHinhEformClient.class );

        CauHinhEformClient cauHinh = appContext.getBean(CauHinhEformClient.class);

        List<CauHinhEformDTO> cauHinhEformList =  cauHinh.getDanhSachCauHinh();

        httpServletRequest.setAttribute("cauHinhEformList", cauHinhEformList);
        httpServletRequest.setAttribute("maForm", maForm);
        httpServletRequest.setAttribute("isImport", isImport);

        httpServletResponse.sendRedirect("/WEB-INF/jsp/taomoiform/cauhinh.jsp");

        super.include(portletConfig, httpServletRequest, httpServletResponse);

    }
}
