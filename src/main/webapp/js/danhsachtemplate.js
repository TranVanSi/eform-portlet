var getDanhSachTemplateURL = $("#getDanhSachTemplateURL").val();
var themMoiURL = $("#themMoiURL").val();

function ajaxRequest(params) {
    var maForm = getMaForm();
    if (maForm == ''){
        maForm = $("#maFormPage").val();
    }
    $.ajax({
        url: getDanhSachTemplateURL
        , data: {
            maForm: maForm,
            offset: params.data.offset,
            limit: params.data.limit
        }
        , cache: false,
        dataType: "json",
        success: function (data) {
            params.success(data);
        },
        error: function (er) {
            params.error(er);
        }
    });
}

function formatterRowIndex(value, row, index) {
    return (Number(getPageNumber()) - 1) * 10 + index + 1;
}

function formatterButtons(value, row, index) {
    var xhtml='';
    xhtml += '<a class="btn fa fa-pencil-square-o" onclick="editRow(\'' + row._id.$oid + '\')" title="Cập nhật thông tin"></a>';
    xhtml += '<a class="btn fa fa-trash-o" onclick="deleteRow(\''+ row._id.$oid +'\')" title="Xóa"></a>';

    return xhtml;
}
function getPageNumber() {
    var pageNumber = 1;
    $('form.form-horizontal').find('table').each(function () {
        var maForm = $(this).attr("id");
        if (typeof maForm != 'undefined' && maForm != "") {
            pageNumber = $('#'+maForm).bootstrapTable('getOptions').pageNumber;
        }
    });
    return pageNumber;
}


function editRow(id){
    var maForm = $("#maForm").val();
    window.location.href=themMoiURL + "&id="+id+"&maForm="+maForm;
}

function deleteRow(id, maForm) {
    if (confirm("Bạn thật sự muốn xóa?")) {
        $("#idDelete").val(id);
        $("#maFormDelete").val(maForm);
        $("#deleteForm").submit();
    }

}

function searchDuLieu() {
    var maForms="";
    $('form.form-horizontal').find('table').each(function () {
        if (typeof $(this).attr("id") != 'undefined') {
            maForms = $(this).attr("id");
        }
    });
    $('#' + maForms).bootstrapTable('destroy');
    $('#' + maForms).bootstrapTable();
}

function getMaForm() {
    var maForm ="";
    $('form.form-horizontal').find('table').each(function () {
        var ma = $(this).attr("id");
        if (typeof ma != 'undefined' && ma != "") {
            maForm = ma;
        }
    });

    return maForm;
}