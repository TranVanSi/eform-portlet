<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://java.sun.com/portlet" prefix="portlet" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>
<%@ taglib uri="http://liferay.com/tld/portlet" prefix="liferay-portlet" %>
<%@ include file="../init.jsp" %>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<portlet:actionURL var="themMoiUrl">
    <portlet:param name="action" value="themMoi"/>
</portlet:actionURL>
<portlet:renderURL var="quayLaiUrl">
    <portlet:param name="action" value=""/>
</portlet:renderURL>
<liferay-portlet:actionURL var="importFileURL">
    <liferay-portlet:param name="action" value="importFile"/>
    <liferay-portlet:param name="maForm" value="${cauHinhEformDTO.maForm}"/>
</liferay-portlet:actionURL>
<liferay-portlet:resourceURL var="urlLoadDanhMuc" id="loadDanhMucDungChung" />
<liferay-portlet:resourceURL var="urlLoadDanhMucEform" id="loadDanhMucEform" />
<liferay-portlet:resourceURL var="searchValidateForm" id="searchValidateForm" />
<liferay-portlet:resourceURL var="urlLoadDonViHanhChinh" id="loadDonViHanhChinhByChaId" />
<liferay-portlet:resourceURL var="loadDataServiceURL" id="loadDataService" />
<liferay-portlet:resourceURL var="getDuLieuCauHinh" id="getDuLieuCauHinh" />
<liferay-portlet:resourceURL var="urlUploadFile" id="uploadFile" />
<liferay-ui:success key="success" message="Yêu cầu thực hiện thành công!"/>
<c:if test="${not empty jspPage}">
    <script>
        window.location="${jspPage}";
    </script>
</c:if>
<input type="hidden" id="urlUploadFile" value="${urlUploadFile}" />
<input type="hidden" id="getDuLieuCauHinh" value="${getDuLieuCauHinh}" />
<input type="hidden" id="urlLoadDanhMuc" value="${urlLoadDanhMuc}" />
<input type="hidden" id="urlLoadDonViHanhChinh" value="${urlLoadDonViHanhChinh}" />
<input type="hidden" id="saveDuLieuFormUrl" value="${saveDuLieuFormUrl}" />
<input type="hidden" id="searchValidateFormUrl" value="${searchValidateForm}" />
<input type="hidden" id="urlLoadDanhMucEform" value="${urlLoadDanhMucEform}" />
<input type="hidden" id="loadDataServiceURL" value="${loadDataServiceURL}" />
<form method="post" name="formImport" id="formImport" action="${importFileURL }"
      enctype='multipart/form-data' class="needs-validation" novalidate="true"
      <c:if test="${isImport == 'false'}">style="display: none"</c:if>>
    <input type="hidden" name="listKeyId" id="listKeyId"/>
    <input type="hidden" name="listKeyMergeId" id="listKeyMergeId"/>
    <div class="file-button">
        <label class="w-100">Import file</label>
        <input class="w-50" name="fileImport" id="fileImport" type="file"
               accept=".xls,.xlsx" title="Import file" onchange="validateImport();">
        <button type="submit" class="btn btn-primary btn-import" disabled>Import dữ liệu</button>
    </div>
</form>
<form action="${themMoiUrl}" method="post" id="formSubmit">
    <input type="hidden" name="objectId" id="objectId" >
    <input type="hidden" name="maForm" id="maForm" value="${cauHinhEformDTO.maForm}" />
    <input type="hidden" name="listId" id="listId" value="${cauHinhEformDTO.listId}" />
    <div class="d-none" type="hidden" name="objResult" id="objResult" >${obj}</div>
    <input type="hidden" id="maFormPage" name="maFormPage" value='${maFormPage}' />
    <input type="hidden" id="dataMapping" name="dataMapping" />
    <input type="hidden" id="addDiChuyen" name="addDiChuyen" value="${addDiChuyen}" />
    <input type="hidden" id="maFormDiChuyen" name="maFormDiChuyen" value="${maFormDiChuyen}" />
    <input type="hidden" id="isXemChiTiet" name="isXemChiTiet" value='${isXemChiTiet}' />
    <input type="hidden" id="typeUpload" name="typeUpload" />
    ${cauHinhEformDTO.html}
    <div class="col-md-12 col-sm-12 col-xs-12 center">
        <button style="display: ${isXemChiTiet eq 'isXemChiTiet' ? 'none' : ""}" class="btn btn-primary" type="button" id="luuThemMoi">Lưu</button>
        <button class="btn btn-primary" type="button" onclick="location.href='${quayLaiUrl}&maFormDanhSach=${maFormPage}'">Quay lại</button>
    </div>
</form>
<div id="openPopup"></div>

<script src="${pageContext.request.contextPath}/js/taomoiform.js"></script>