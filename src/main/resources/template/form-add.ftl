<form class="form-horizontal" enctype="multipart/form-data">
    <fieldset>
        <div class="row">
            <legend class="center">${formName}</legend>
            ${bodyHtml}
        </div>
    </fieldset>
</form>