package vn.sdt.eform.cauhinheform.services;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.liferay.portal.kernel.util.Validator;
import freemarker.template.Configuration;
import freemarker.template.Template;
import lombok.RequiredArgsConstructor;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import vn.sdt.eform.cauhinheform.client.CRUDServiceClient;
import vn.sdt.eform.cauhinheform.client.CauHinhEformClient;
import vn.sdt.eform.cauhinheform.dto.*;
import vn.sdt.eform.utils.Constants;
import vn.sdt.eform.utils.EformUtils;

import java.io.File;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@Service
@RequiredArgsConstructor
public class CauHinhEformService {
    private final CauHinhEformClient cauHinhEformClient;
    private final CRUDServiceClient crudServiceClient;

    @Autowired
    @Qualifier("objectMapper")
    public ObjectMapper objectMapper;

    public Page<CauHinhEformDTO> getDanhSachCauHinh(int offset, int limit, String search) {
        return cauHinhEformClient.getDanhSachCauHinh(offset, limit, search);
    }

    public CauHinhEformDTO findByMa(String ma) {
        return cauHinhEformClient.findByMa(ma);
    }

    public CauHinhEformDTO saveCauHinhEform(CauHinhEformDTO cauHinhEformDTO) {
        return cauHinhEformClient.savecauHinhForm(cauHinhEformDTO);
    }

    public void deleteCauHinhEform(String maForm) {
        cauHinhEformClient.deleteCauHinhEform(maForm);
    }

    public List<CauHinhEformDTO> getDanhSach() {
        return cauHinhEformClient.getDanhSachCauHinh();
    }

    public String getInfoUpload(File file, String maForm, String tenForm, String loaiForm) throws Exception {
        ResultDTO resultDTO = getDataByPostmanJson(file);
        Object objRequest = resultDTO.getItem().get(0).getRequest();
        objectMapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
        RequestDTO requestDTO = objectMapper.readValue(objectMapper.writeValueAsString(objRequest), new TypeReference<RequestDTO>() {
        });
        CauHinhEformDTO cauHinhEform = new CauHinhEformDTO();
        if (Integer.parseInt(loaiForm) == Constants.LOAI_FORM.CREATE_SERVICE) {
            cauHinhEform = getInfoCreateService(requestDTO, maForm, tenForm, Integer.parseInt(loaiForm));
        }
        if (Integer.parseInt(loaiForm) == Constants.LOAI_FORM.DANHSACH_SERVICE){
            cauHinhEform = getInfoDanhSachService(resultDTO.getItem().get(0), maForm, tenForm, Integer.parseInt(loaiForm));
        }
        String token = "";
        if (Validator.isNotNull(requestDTO.getAuth())) {
            if (requestDTO.getAuth().has("bearer")) {
                token = requestDTO.getAuth().get("bearer").get(0).get("value").toString().substring(1, requestDTO.getAuth().get("bearer").get(0).get("value").toString().length()-1);
            }
        }
        saveCauHinhCRUDService(cauHinhEform.getId(), objectMapper.writeValueAsString(resultDTO),token);

        return cauHinhEform.getId();
    }

    private CauHinhEformDTO getInfoCreateService(RequestDTO requestDTO, String maForm, String tenForm, int loaiForm) throws Exception {
        String jsonBody = requestDTO.getBody().get("raw").toString();
        JSONObject js = objectMapper.readValue(jsonBody, JSONObject.class);
        Iterator<String> keys = js.keys();
        List<String> list = new ArrayList<>();
        while (keys.hasNext()) {
            list.add(keys.next());
        }
        Configuration freemarkerConfig = new Configuration();
        freemarkerConfig.setClassForTemplateLoading(this.getClass(), "/template");
        Template input = freemarkerConfig.getTemplate("input-add.ftl");
        Template inputCollection = freemarkerConfig.getTemplate("input-collection.ftl");
        Template form = freemarkerConfig.getTemplate("form-add.ftl");
        Template formCollection = freemarkerConfig.getTemplate("form-collection.ftl");
        List<String> listId = new ArrayList<>();
        List<String> inputCollections = new ArrayList<>();
        String html = "";
        for (String itemStr : list) {
            listId.add(itemStr);
            inputCollections.add(EformUtils.createInputCollectionSetting(inputCollection, itemStr));
            html += EformUtils.createInputHtml(input, itemStr);
        }
        String collectionBody = EformUtils.createCollectionSetting(formCollection, Constants.TEN_FORM_DEFAULT, String.join(",", inputCollections));
        html = EformUtils.createBodyAddForm(form, html, Constants.TEN_FORM_DEFAULT);

        CauHinhEformDTO cauHinhEform = new CauHinhEformDTO(null, maForm, tenForm,
                true, html, String.join(",", listId), collectionBody, loaiForm);
        cauHinhEform = saveCauHinhEform(cauHinhEform);
        return cauHinhEform;
    }

    private CauHinhEformDTO getInfoDanhSachService(ItemPostmanDTO itemPostmanDTO, String maForm, String tenForm, int loaiForm) throws Exception {
        JsonNode jsonBody = itemPostmanDTO.getResponse();
        JSONObject item = new JSONObject(objectMapper.writeValueAsString(jsonBody.get(0)));
        Iterator<String> keys = item.keys();
        List<String> list = new ArrayList<>();
        while (keys.hasNext()) {
            list.add(keys.next());
        }
        Configuration freemarkerConfig = new Configuration();
        freemarkerConfig.setClassForTemplateLoading(this.getClass(), "/template");
        Template th = freemarkerConfig.getTemplate("th-table.ftl");
        Template tableCollection = freemarkerConfig.getTemplate("table-collection.ftl");
        Template table = freemarkerConfig.getTemplate("table.ftl");
        String html = "";
        List<String> listId = new ArrayList<>();
        String listStr = Constants.CREATE_COLLECTION_TABLE.STT;
        for (String itemStr : list) {
            listId.add(itemStr);
            html += EformUtils.createThTable(th, itemStr);
            listStr += Constants.CREATE_COLLECTION_TABLE.ENCODE_DAU_NHAY + itemStr + Constants.CREATE_COLLECTION_TABLE.ENCODE_DAU_NHAY + ",";
        }
        listStr = listStr.substring(0, listStr.length() - 1);
        String collectionBody = EformUtils.createTableCollection(tableCollection, "[" + listStr + "]", Constants.TEN_FORM_DEFAULT);
        html = EformUtils.createTable(table, Constants.TEN_FORM_DEFAULT, html);
        CauHinhEformDTO cauHinhEform = new CauHinhEformDTO(null, maForm, tenForm,
                true, html, String.join(",",listId), collectionBody, loaiForm);
        cauHinhEform = saveCauHinhEform(cauHinhEform);
        return cauHinhEform;
    }

    public ResultDTO getDataByPostmanJson(File file) throws Exception {
        String obj = "";
        BufferedReader br = new BufferedReader(new FileReader(file.getPath()));
        while (br.ready()) {
            obj = obj.concat(br.readLine());
        }
        objectMapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
        return objectMapper.readValue(obj, ResultDTO.class);
    }

    public void saveCauHinhCRUDService(String id, String dataJson, String token) {
        ServiceDataDTO serviceDataDTO = new ServiceDataDTO(null, id, dataJson, token);
        crudServiceClient.saveCauHinhCRUDService(serviceDataDTO);
    }

    public void deleteCauHinhCRUDService(String id) {
        crudServiceClient.deleteCauHinh(id);
    }

    public ServiceDataDTO findByCauHinhId(String cauHinhId) {
        return crudServiceClient.findByCauHinhId(cauHinhId);
    }

    public CauHinhEformDTO findById(String id) {
        return cauHinhEformClient.findById(id);
    }

    public ResponseDTO uploadFileEform(File file) {
        return cauHinhEformClient.uploadFileEform(file);
    }

    public List<CauHinhEformDTO> getDanhSachCauHinhByLoai(int loai) {
        return cauHinhEformClient.getDanhSachCauHinhByLoai(loai);
    }
}
