<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://liferay.com/tld/portlet" prefix="liferay-portlet" %>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui" %>

<portlet:defineObjects />

<form action='<liferay-portlet:actionURL portletConfiguration="true"/>' method="post" class="form-control">
    <h1 class="center septop20 sepbot20 upper">
        Chọn mã eform cấu hình<br/>
    </h1>
    <div class="fi">
        <div class="fi-wrapper">
            <div class="fi-input">
                <aui:select class="form-control" name="maForm" id="maForm">
                    <c:forEach items="${cauHinhEformList}" var="cauHinh">
                        <option value="${cauHinh.maForm}" <c:if test="${cauHinh.maForm == maForm}">selected</c:if>> ${cauHinh.tenForm}</option>
                    </c:forEach>
                </aui:select>
            </div>
            <div class="fi-input">
                <aui:select class="form-control" name="isImport" id="isImport">
                    <option value="true" <c:if test="${isImport == 'true'}">selected</c:if>>Hiện</option>
                    <option value="false" <c:if test="${isImport == 'false'}">selected</c:if>>Ẩn</option>
                </aui:select>
            </div>
        </div>
    </div>

    <div class="hidden-sep">&nbsp;</div>
    <div class="button-bar">
        <button class="btn btn-outline-primary" type="submit">
            Lưu cấu hình
        </button>
    </div>
</form>