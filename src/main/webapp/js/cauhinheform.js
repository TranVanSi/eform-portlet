var xoaURL = $('#xoaUrl').val();
var chinhSuaUrl = $('#chinhSuaUrl').val();
var danhSachDangKyUrl = $('#danhSachDangKyUrl').val();
var inURL = $('#inURL').val();

function enableBtn() {
    if ($('#fileImport').val().length > 0) {
        $('.btn-import').prop('disabled','');
    }
}
$(function () {
    setTimeout(function () {
        initTable();
        if ($(window).width() < 415) {
            document.getElementsByName("toggle")[0].click();
        }
        $('#buttonadd').prop('style','display:inline');
        $('#title-table').prop('style','display:inline');
        $('#table').prop('class','table table-striped');
    }, 100);

});
function indexFormatter(value, row, index) {
    return ($('#table').bootstrapTable('getOptions').pageNumber - 1) * $('#table').bootstrapTable('getOptions').pageSize + index + 1;
}

function operateFormatter(value, row, index) {
    return [
        '<a class="capnhat" href="#" title="Cập nhật">',
        '<i class="fa-capnhat"></i>',
        '</a>',
        '<a class="remove" href="#" title="Xóa">',
        '<i class="fa-rmv"></i>',
        '</a>',
        '<a class="cauHinhIn" href="#" title="thêm form in">',
        '<i class="fa fa-print"></i>',
        '</a>'
    ].join('');
}


window.operateEvents = {
    'click .capnhat': function (e, value, row, index) {
        location.href = chinhSuaUrl+'&maEform=' + [row.maForm];
    },
    'click .remove': function (e, value, row, index) {
        if (confirm('Bạn có thật sự muốn xóa?')) {
            location.href = xoaURL+'&maEform=' + [row.maForm];
        }
    },
    'click .cauHinhIn': function (e, value, row, index) {
        location.href = inURL+'&maForm=' + [row.maForm];
    }
}

function initTable() {

    $('#table').bootstrapTable('destroy').bootstrapTable({
        columns: [
            [{
                title: 'STT',
                field: 'stt',
                align: 'center',
                valign: 'middle',
                width: '5%',
                formatter: indexFormatter
            }, {
                title: 'Mã',
                field: 'maForm',
                align: 'center',
                valign: 'middle',
                width: '10%'
            }, {
                title: 'Tên Eform',
                field: 'tenForm',
                align: 'center',
                valign: 'middle',
                width: '20%'
            }, {
                title: 'Trạng thái',
                field: 'trangThaiForm',
                align: 'center',
                valign: 'middle',
                width: '10%',
                formatter: 'getTenTrangThai'
            }, {
                field: 'id',
                title: 'Thao tác',
                align: 'center',
                clickToSelect: false,
                width: '10%',
                events: window.operateEvents,
                formatter: operateFormatter
            }]
        ]
    });
}
function getTenTrangThai(trangThai) {
    var name = "";
    if (trangThai === true){
        name = "Chia sẻ";
    }else {
        name = "Không chia sẻ";
    }

    return name;
}

