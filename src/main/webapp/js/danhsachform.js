var getUrlCauHinh = $("#getUrlCauHinh").val();
var filterUrl = $("#filterUrl").val();
var inURL = $("#inURL").val();
var searchDuLieuFormUrl = $("#searchDuLieuFormUrl").val();
var tenTableCauHinh = "CHURL";
var maFormTemplate = "TEMPLATE_PRINT";
var dulieuTemplateIn;
$(document).ready(function () {
    loadDanhMucDungChung();
    var tinhThanhSelected = $("#tinhThanhSelected").val();
    if (tinhThanhSelected > 0){
        redrawSelectBox("quanHuyenTruQuanChuHoSoSearch", tinhThanhSelected);
    }
    $('input').keydown(function(e){
        if (e.keyCode == 13) {
            searchDuLieu();
            return false;
        }
    });
    getButtonIn();
    $("#maTenSearch").parent().parent().find('label').prop("style","color:white")
    $("#btnSearch").parent().parent().prop("style","padding-top: 34px;");
    $("#btnThemMoi").parent().parent().prop("style","padding-top: 34px;");
});

function loadDanhMucDungChung() {
    $('form.form-horizontal fieldset div.row').find('select').each(function () {
        var elementId = $(this).attr("id");
        redrawSelectBoxOnLoad(elementId);
    });
}

function redrawSelectBoxOnLoad(elementId) {
    var url = $('#urlLoadDanhMucEform').val();
    var eform = $("#" + elementId).attr("eform");
    var valueOption = $("#" + elementId).attr("key-eform");
    var labelOption = $("#" + elementId).attr("label-eform");
    var filterKey = $("#" + elementId).attr("filter_key_eform");
    var tinhThanhSelected = $("#tinhThanhSelected").val();
    if (typeof filterKey != "undefined" && filterKey == 0) {
        if (typeof eform != undefined && eform.length > 0) {
            $.ajax({
                type: 'GET'
                , url: url
                , async: false
                , contentType: "application/json; charset=utf-8"
                , data: {
                    maForm: eform,
                    valueOption: valueOption,
                    labelOption: labelOption
                }
                , success: function (data) {
                    if (data !== null) {
                        $(this).empty();
                        var xhtml = "<option value='0'>---Chọn---</option>";
                        $.map(JSON.parse(JSON.stringify(data)), function (value, key) {
                            var selected = "";
                            if (typeof tinhThanhSelected != 'undefined' && key == tinhThanhSelected){
                                selected = "selected";
                            }
                            xhtml += "<option value='" + key + "' "+selected+">" + value + "</option>";
                        });
                        $("#" + elementId).html(xhtml);
                        if (tinhThanhSelected > 0){
                            redrawSelectBox("quanHuyenTruQuanChuHoSoSearch", tinhThanhSelected);
                        }
                    }
                }
            });
        }
    }
}

function ajaxRequest(params) {
    var maForm = getMaForm();
    if (maForm == ''){
        maForm = $("#maFormPage").val();
    }
    var listTruongSearch = "";
    var listValueSearch = "";
    $("input[id$='Search']").each(function () {
        var nameTruong = $(this).attr('name');
        var valueTruong = $(this).val();
        if (nameTruong != "" && typeof nameTruong != 'undefined' && valueTruong.length > 0){
            listTruongSearch += nameTruong.substring(0, nameTruong.length-6)+",";
            listValueSearch += valueTruong +",";
        }
    })
    $("select[id$='Search']").each(function () {
        var nameTruong = $(this).attr('name');
        var valueTruong = $(this).val();
        if (nameTruong != "" && typeof nameTruong != 'undefined' && valueTruong.length > 0 && parseInt(valueTruong) > 0){
            listTruongSearch += nameTruong.substring(0, nameTruong.length-6)+",";
            listValueSearch += valueTruong +",";
        }
    })

    if (listTruongSearch != ""){
        listTruongSearch = listTruongSearch.substring(0, listTruongSearch.length-1);
        listValueSearch = listValueSearch.substring(0, listValueSearch.length-1);
    }

    $.ajax({
        url: filterUrl
        , data: {
            maForm: maForm,
            offset: params.data.offset,
            limit: params.data.limit,
            listTruongSearch: listTruongSearch,
            listValueSearch: listValueSearch
        }
        , cache: false,
        dataType: "json",
        success: function (data) {
            params.success(data);
        },
        error: function (er) {
            params.error(er);
        }
    });
}

function formatterRowIndex(value, row, index) {
    return (Number(getPageNumber()) - 1) * 10 + index + 1;
}

var bien;
function formatterCell(value, row, index, cellName) {
    var dataMapping = row.dataMapping;
    if (cellName.toString().indexOf("-") > -1){
        var items = cellName.split("-");
        var kq = "";
        for (var i=0; i <items.length; i ++){
            var result = items[i];

            if (typeof dataMapping[result] != 'undefined' && dataMapping[result] != '---Chọn---' && dataMapping[result] != ''){
                kq += dataMapping[result]+", ";
            }else{
                if (typeof row[result] != 'undefined' && row[result] != '' ) {
                    kq += row[result] + ", ";
                }
            }
        }
        if (kq !=''){
            return  kq.substring(0, kq.length-2);
        }
    }
    if (cellName.toString().indexOf("|") > -1){
        var arr = cellName.split("|");
        var rs = "";
        var str = row[arr[0]];
        var arrList = "";
        if (typeof str != 'undefined') {
            if (arr[arr.length - 1] == 'n') {
                arrList = str[str.length - 1];
            } else {
                arrList = str[0];
            }
            if (typeof arrList != 'undefined') {
                if (typeof arrList.dataMapping != 'undefined') {
                    var dataMp = arrList.dataMapping;
                    if (typeof dataMp[arr[1]] != 'undefined' && dataMapping[arr[1]] != '---Chọn---' && dataMapping[arr[1]] != '') {
                        rs = dataMp[arr[1]];
                    }
                }
                var parsedDate = Date.parse(arrList[arr[1]]);
                if (isNaN(arrList[arr[1]]) && !isNaN(parsedDate)) {
                    return arrList[arr[1]].substring(8) + "/" + arrList[arr[1]].substring(5, 7) + "/" + arrList[arr[1]].substring(0, 4);
                }
            }
        }
        return rs;
    }
    if (typeof dataMapping[cellName] != 'undefined' && dataMapping[cellName] != '---Chọn---' && dataMapping[cellName] != ''){
        return dataMapping[cellName];
    }
    var parsedDate = Date.parse(value);
    if (isNaN(value) && !isNaN(parsedDate)) {
        return value.substring(8)+"/"+value.substring(5,7)+"/"+value.substring(0,4);
    }
    return value;
}

function isDate(dateStr) {
    return !isNaN(new Date(dateStr).getDate());
}

function formatterButtons(value, row, index) {
    var maForm = getMaForm();
    var maFormPage = $("#maFormPage").val();
    var id = row._id.$oid;
    var xhtml='';
    $.ajax({
        url: getUrlCauHinh
        , data: {
            maForm: maForm,
            tenTable: tenTableCauHinh
        }
        , cache: false,
        async: false,
        dataType: "json",
        success: function (data) {
            for (var i = 0; i < data.data.length; i ++){
                var item = JSON.parse((data.data)[i]);

                if (item['tenButton'] == 'themmoi'){
                    var linkChinhSua = location.protocol + '//' + location.host +item['linkUrl']+"?idEdit="+id+"&maFormEdit="+maForm+"&loai=1&maFormPage="+maFormPage;
                    xhtml += '<a class="btn fa fa-pencil-square-o" onclick="location.href=\''+linkChinhSua+'\'" title="Cập nhật thông tin"></a>';
                }
                if (item['tenButton'] == 'chitiet'){
                    var linkChiTiet = location.protocol + '//' + location.host +item['linkUrl']+"?idChiTiet="+id+"&maFormChiTiet="+maForm+"&loaiChiTiet=1&xemChiTiet=1&maFormPage="+maFormPage;
                    xhtml += '<a class="btn fa fa-info-circle" onclick="location.href=\''+linkChiTiet+'\'" title="Xem chi tiết"></a>';
                }
                if (item['tenButton'] == 'chinhsach'){
                    var linkChinhSach = location.protocol + '//' + location.host +item['linkUrl']+"?idChinhSach="+id+"&maFormPage="+maFormPage;
                    xhtml += '<a class="btn fa fa-address-card-o" onclick="location.href=\''+linkChinhSach+'\'" title="Chính sách"></a>';
                }
                if (item['tenButton'] == 'dichuyen'){
                    var linkDiChuyen = location.protocol + '//' + location.host +item['linkUrl']+"?idDiChuyen="+id+"&maFormDiChuyen="+maForm+"&maFormPage="+maFormPage;
                    xhtml += '<a class="btn fa fa-bus" onclick=" location.href=\''+linkDiChuyen+'\'" title="Di chuyển"></a>';
                }
            }
            xhtml += '<a class="btn fa fa-trash-o" onclick="deleteRow(\''+ row._id.$oid +'\',\''+ maForm +'\')" title="Xóa"></a>';
            if (dulieuTemplateIn != null && dulieuTemplateIn.length > 0) {
                dulieuTemplateIn.forEach(function (element) {
                    xhtml += '<a class="btn fa fa-print" onclick="inForm(\''+ JSON.parse(element)._id.$oid +'\',\''+ row._id.$oid +'\',\''+ maForm +'\')" title="' + JSON.parse(element).tenForm + '"></a></td>';
                });
            }
            xhtml += '</td>';
        },
        error: function (er) {
        }
    });

    return xhtml;
}

function getPageNumber() {
    var pageNumber = 1;
    $('form.form-horizontal').find('table').each(function () {
        var maForm = $(this).attr("id");
        if (typeof maForm != 'undefined' && maForm != "") {
            pageNumber = $('#'+maForm).bootstrapTable('getOptions').pageNumber;
        }
    });
    return pageNumber;
}


function editRow(id, maForm, maCauHinh){
    var maFormPage = $("#maFormPage").val();
    $.ajax({
        url: getUrlCauHinh
        , data: {
            maForm: maCauHinh,
            tenTable: tenTableCauHinh
        }
        , cache: false,
        async: true,
        dataType: "json",
        success: function (data) {
            var url = JSON.parse(data.data[0]).linkUrl;
            window.location.href=location.protocol + '//' + location.host +url+"?idEdit="+id+"&maFormEdit="+maForm+"&loai=1&maFormPage="+maFormPage;
        },
        error: function (er) {
        }
    });


}

function chiTietRow(id, maForm, maCauHinh){
    var maFormPage = $("#maFormPage").val();
    $.ajax({
        url: getUrlCauHinh
        , data: {
            maForm: maCauHinh,
            tenTable: tenTableCauHinh
        }
        , cache: false,
        async: true,
        dataType: "json",
        success: function (data) {
            var url = JSON.parse(data.data[0]).linkUrl;
            window.location.href=location.protocol + '//' + location.host +url+"?idChiTiet="+id+"&maFormChiTiet="+maForm+"&loaiChiTiet=1&xemChiTiet=1&maFormPage="+maFormPage;
        },
        error: function (er) {
        }
    });

}


function inForm(id, objectId, maForm){
    location.href=inURL + '&id=' + id + '&objectId=' + objectId + '&maForm=' + maForm;
}

function deleteRow(id, maForm) {
    if (confirm("Bạn thật sự muốn xóa?")) {
        $("#idDelete").val(id);
        $("#maFormDelete").val(maForm);
        $("#deleteForm").submit();
    }

}

 $("#btnThemMoi").click(function () {
     var maFormPage = $("#maFormPage").val();
     var maFormThemMoi = getMaThemmoi();
     $.ajax({
         url: getUrlCauHinh
         , data: {
             maForm: maFormThemMoi,
             tenTable: tenTableCauHinh
         }
         , cache: false,
         async: true,
         dataType: "json",
         success: function (data) {
             for (var i = 0; i < data.data.length; i ++) {
                 var item = JSON.parse((data.data)[i]);
                 if (item['tenButton'] == 'themmoi') {
                     window.location.href = location.protocol + '//' + location.host + item['linkUrl'] + "?loai=1&maFormPage=" + maFormPage;
                 }
             }
         },
         error: function (er) {
         }
     });
 });


function searchDuLieu() {
    var maForms="";
    $('form.form-horizontal').find('table').each(function () {
        if (typeof $(this).attr("id") != 'undefined') {
            maForms = $(this).attr("id");
        }
    });
    $('#' + maForms).bootstrapTable('destroy');
    $('#' + maForms).bootstrapTable();
}

function getMaForm() {
    var maForm ="";
    $('form.form-horizontal').find('table').each(function () {
        var ma = $(this).attr("id");
        if (typeof ma != 'undefined' && ma != "") {
            maForm = ma;
        }
    });

    return maForm;
}

function getMaThemmoi() {
    var ma ="";
    $('form.form-horizontal').find('table').each(function () {
        var link = $(this).attr("linkthemmoi");
        if (typeof link != 'undefined' && link != "") {
            ma = link;
        }
    });
    return ma;
}

function getMaChiTiet() {
    var ma ="";
    $('form.form-horizontal').find('table').each(function () {
        var link = $(this).attr("linkchitiet");
        if (typeof link != 'undefined' && link != "") {
            ma = link;
        }
    });
    return ma;
}

function redrawSelectBox(elementId, queryParam) {
    var url = $('#urlLoadDanhMucEform').val();
    var eform = $("#" + elementId).attr("eform");
    var valueOption = $("#" + elementId).attr("key-eform");
    var labelOption = $("#" + elementId).attr("label-eform");
    var filterKey = $("#" + elementId).attr("filter_key_eform");

    if (typeof eform != undefined && eform.length > 0) {
        $.ajax({
            type: 'GET'
            , url: url
            , async: false
            , contentType: "application/json; charset=utf-8"
            , data: {
                maForm: eform,
                valueOption: valueOption,
                labelOption: labelOption,
                filterKey: filterKey,
                filterValue: queryParam
            }
            , success: function (data) {
                if (data !== null) {
                    $(this).empty();
                    var xhtml = "<option value='0'>---Chọn---</option>";
                    $.map(JSON.parse(JSON.stringify(data)), function (value, key) {
                        xhtml += "<option value='" + key + "'>" + value + "</option>";
                    });
                    $("#" + elementId).html(xhtml);
                }
            }
        });
    }
}

function getButtonIn() {
    var maForm = getMaForm();
    $.ajax({
        url: searchDuLieuFormUrl
        , data: {
            maForm: maFormTemplate,
            listTruongSearch: "maForm",
            listValueSearch: maForm
        }
        , cache: false,
        dataType: "json",
        success: function (data) {
            dulieuTemplateIn = data;
        }
    });
}