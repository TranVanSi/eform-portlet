<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://java.sun.com/portlet" prefix="portlet" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>
<%@ taglib uri="http://liferay.com/tld/portlet" prefix="liferay-portlet" %>
<%@ include file="../init.jsp" %>
<script src="${pageContext.request.contextPath}/js/cauhinheform.js"></script>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/css/cauhinheform.css">
</head>
<portlet:renderURL var="themMoiUrl">
    <portlet:param name="action" value="themMoi"/>
</portlet:renderURL>
<portlet:renderURL var="getDanhSachDangKyUrl">
    <portlet:param name="action" value="getDanhSachDangKy"/>
</portlet:renderURL>
<liferay-portlet:actionURL var="xoaUrl">
    <portlet:param name="action" value="xoa"></portlet:param>
</liferay-portlet:actionURL>
<liferay-portlet:actionURL var="importFileURL">
    <liferay-portlet:param name="action" value="importFile"/>
</liferay-portlet:actionURL>
<liferay-portlet:resourceURL var="getDanhSachEform" id="getDanhSachEform"></liferay-portlet:resourceURL>

<portlet:renderURL var="inURL">
    <portlet:param name="action" value="in"/>
</portlet:renderURL>
<liferay-ui:success key="success" message="Yêu cầu thực hiện thành công!"/>
<liferay-ui:success key="form-success" message="${thongBao}"/>

<input type="hidden" id="xoaUrl" value="${xoaUrl}" />
<input type="hidden" id="chinhSuaUrl" value="${themMoiUrl}" />
<input type="hidden" id="danhSachDangKyUrl" value="${getDanhSachDangKyUrl}" />
<input type="hidden" id="inURL" value="${inURL}" />
<main class="main-table-custom">
    <div class="col-md-12 col-sm-12 col-xs-12 center">
        <label class="tieuDe">DANH SÁCH EFORM</label>
    </div>
    <div class="file-button">
        <button style="float: right" class="btn btn-primary" onclick="location.href='${themMoiUrl}'">Thêm mới</button>
    </div>
    <table id="table" data-url="${getDanhSachEform}" class="table table-striped"
           data-side-pagination="server"
           data-pagination="true"
           data-search="true"
           data-toolbar="#toolbar"
           data-show-columns="false"
           data-show-pagination-switch="false"
           data-show-refresh="false"
           data-key-events="false"
           data-show-toggle="true"
           data-resizable="false"
           data-cookie="false"
           data-cookie-id-table="saveId"
           data-show-fullscreen="false"
           data-detail-view="false"
           data-show-export="false"
           data-click-to-select="false">
    </table>
</main>

<script src="${pageContext.request.contextPath}/js/cauhinheform.js"></script>