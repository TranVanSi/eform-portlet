<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://java.sun.com/portlet" prefix="portlet" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>
<%@ taglib uri="http://liferay.com/tld/portlet" prefix="liferay-portlet" %>

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>

<liferay-portlet:renderURL var="editUrl">
    <portlet:param name="action" value="edit"/>
</liferay-portlet:renderURL>
<liferay-portlet:renderURL var="chiTietUrl">
    <portlet:param name="action" value="chiTiet"/>
</liferay-portlet:renderURL>

<liferay-portlet:actionURL var="deleteUrl">
    <portlet:param name="action" value="delete"/>
</liferay-portlet:actionURL>
<liferay-portlet:resourceURL var="getDuLieuTableUrl" id="getDuLieuTable" />
<liferay-portlet:resourceURL var="searchDuLieuFormUrl" id="searchDuLieuForm" />
<liferay-portlet:resourceURL var="filterUrl" id="filter">
</liferay-portlet:resourceURL>
<liferay-ui:success key="success" message="Yêu cầu thực hiện thành công!"/>
<c:if test="${not empty jspPage}">
    <script>
        window.location="${jspPage}";
    </script>
</c:if>
<c:choose>
    <c:when test="${loai == '1' || loai == '4'}">
        <jsp:include
                page="${request.contextPath}/WEB-INF/jsp/crudform/taomoi.jsp"></jsp:include>
    </c:when>
    <c:when test="${loai == '2' || loai == '5'}">
        <jsp:include
                page="${request.contextPath}/WEB-INF/jsp/crudform/danhsach.jsp"></jsp:include>
    </c:when>
    <c:otherwise>
        <jsp:include
                page="${request.contextPath}/WEB-INF/jsp/crudform/chitiet.jsp"></jsp:include>
    </c:otherwise>
</c:choose>
