package vn.sdt.eform.cauhinheform.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CauHinhEformDTO {
    private String id;
    private String maForm;
    private String tenForm;
    private boolean trangThaiForm;
    private String html;
    private String listId;
    private String collection;
    private int loai;
}
