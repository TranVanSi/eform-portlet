<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<%@ include file="../init.jsp" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<style>
    .chosen-multi{
        border: 1px solid #5897fb;
        box-shadow: 0 0 5px rgb(0 0 0 / 30%);
        position: relative;
        overflow: hidden;
        margin: 0;
        padding: 0 5px;
        width: 100%;
        height: auto !important;
    }
    .choice {
        float: left;
        list-style: none;
        position: relative;
        margin: 3px 5px 3px 0;
        padding: 3px 5px 3px 5px;
        border: 1px solid #aaa;
        max-width: 100%;
        border-radius: 3px;
        background-image: linear-gradient(#f4f4f4 20%, #f0f0f0 50%, #e8e8e8 52%, #eeeeee 100%);
        color: #333;
        line-height: 13px;
        cursor: grab;
    }
</style>

<liferay-portlet:actionURL var="themMoiInURL">
    <liferay-portlet:param name="action" value="themMoiIn"/>
</liferay-portlet:actionURL>

<html lang="en">
<meta http-equiv="content-type" content="text/html;charset=UTF-8"/>
<head>
    <meta charset="utf-8">
    <title>Eform</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/css/themmoicauhinh.css">
</head>
<div id="loading" style="display:none">
    <img src="${pageContext.request.contextPath}/images/loading.gif" alt="Loading..."/>
</div>
<body>
<div id="busybox" class="bg">
    <div></div>
</div>
<main class="main-table-custom main-themmoigiayto">
    <div class="col-md-12 col-sm-12 col-xs-12 center">
        <label class="tieuDe">THIẾT KẾ MÀN IN HỒ SƠ</label>
    </div>
    <div class="container cauHinhEform text-left" id="cauHinhEform">
        <form:form action="${themMoiInURL}" method="post" id="formLuuEformIn" modelAttribute="item">
            <form:input path="id" cssStyle="display: none"/>
            <form:input path="listId" cssStyle="display: none"/>
            <div id="main" class="row">
                <div class="col-md-4 col-sm-4 col-xs-12">
                    <label class="text">Mã tiện ích<span class="red">*</span></label>
                    <form:input path="maForm" class="form-control mb-2" />
                </div>
                <div class="col-md-4 col-sm-4 col-xs-12">
                    <label class="text">Tên tiện ích<span class="red">*</span></label>
                    <form:input path="tenForm" class="form-control mb-2" />
                </div>
            </div>
            <div class="row">
                <div class="col-12 mb-2">
                    <ul class="chosen-multi">
                        <c:forEach items="${listIds}" var="item">
                            <li id="${item}" value="${item}" draggable="true" onclick="copyToClipboard(${item})" class="choice">${item}</li>
                        </c:forEach>
                    </ul>
                </div>
                <div class="col-12">
                    <liferay-ui:input-editor name="html" initMethod="initEditor" height="600"
                                             resizable="true" contents="${item.html}"></liferay-ui:input-editor>
                </div>
                <div class="col-12">
                    <button type="submit" id="luueform" class="btn btn-primary success">Lưu cấu hình</button>
                    <button type="button" class="btn btn-primary success" id="btnQuayLai" >Quay lại</button>
                </div>
            </div>
        </form:form>
    </div>
</main>

</body>
<script>
    function initEditor(){
        return $(this).attr('contents');
    }

    function copyToClipboard(element) {
        var $temp = $("<input>");
        $("body").append($temp);
        $temp.val('\${' + $(element).text() + '}').select();
        document.execCommand("copy");
        $temp.remove();
    }


</script>
</html>
