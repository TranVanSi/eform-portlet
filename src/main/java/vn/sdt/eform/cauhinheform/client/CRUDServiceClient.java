package vn.sdt.eform.cauhinheform.client;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;
import vn.sdt.eform.cauhinheform.dto.ServiceDataDTO;
import vn.sdt.eform.cauhinheform.dto.ResponseDTO;
import vn.sdt.eform.utils.Constants;

@Service
@RequiredArgsConstructor
public class CRUDServiceClient {
    private final String path = "/api/v1/servicedata";
    @Autowired
    @Qualifier("eformRest")
    public RestTemplate restTemplate;

    public ServiceDataDTO saveCauHinhCRUDService(ServiceDataDTO serviceDataDTO) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON_UTF8);
        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(path);

        HttpEntity<ServiceDataDTO> entity = new HttpEntity<>(serviceDataDTO, headers);
        try {
            ResponseEntity<ResponseDTO<ServiceDataDTO>> responseEntity = restTemplate.exchange(
                    builder.toUriString(), HttpMethod.POST, entity, new ParameterizedTypeReference<ResponseDTO<ServiceDataDTO>>() {
                    });
            if (responseEntity.getBody().getStatus() == Constants.RESPONE.SUCCESS) {
                return responseEntity.getBody().getData();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    public ServiceDataDTO findByCauHinhId(String cauHinhId) {
        String restUrl = path + "/findByCauHinhId";
        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(restUrl)
                .queryParam("cauHinhId", cauHinhId);
        try {
            ResponseEntity<ResponseDTO<ServiceDataDTO>> responseEntity = restTemplate.exchange(
                    builder.toUriString(), HttpMethod.GET, null, new ParameterizedTypeReference<ResponseDTO<ServiceDataDTO>>() {
                    });

            if (responseEntity.getBody().getStatus() == Constants.RESPONE.SUCCESS) {
                return responseEntity.getBody().getData();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    public boolean deleteCauHinh(String cauHinhId) {
        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(path + "/delete").queryParam("cauHinhId", cauHinhId);
        HttpEntity<?> response = restTemplate.exchange(builder.toUriString(), HttpMethod.DELETE, null, String.class, new Object[0]);

        return ((ResponseEntity<?>) response).getStatusCode().is2xxSuccessful();
    }
}
