package vn.sdt.eform.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.core.env.Environment;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.DefaultUriTemplateHandler;

@Configuration
@RequiredArgsConstructor
@PropertySource("classpath:config.properties")
public class Config {
    private final Environment env;

    @Bean
    public static PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer() {
        return new PropertySourcesPlaceholderConfigurer();
    }

    @Bean("eformRest")
    public RestTemplate templateEform() {
        String rootURL = env.getProperty("api.v1.url.eform");

        RestTemplate restClient = new RestTemplate();
        DefaultUriTemplateHandler uriTemplateHandler = new DefaultUriTemplateHandler();
        uriTemplateHandler.setBaseUrl(rootURL);
        restClient.setUriTemplateHandler(uriTemplateHandler);

        return restClient;
    }

    @Bean("objectMapper")
    public ObjectMapper getObjectMapper() {
        return new ObjectMapper();
    }

    @Bean("lgspRest")
    public RestTemplate restLGSPRest() {
        String rootURL = env.getProperty("api.v1.url.lgsp");

        RestTemplate restClient = new RestTemplate();
        DefaultUriTemplateHandler uriTemplateHandler = new DefaultUriTemplateHandler();
        uriTemplateHandler.setBaseUrl(rootURL);
        restClient.setUriTemplateHandler(uriTemplateHandler);

        return restClient;
    }

    @Bean("uploadAPIRest")
    public RestTemplate uploadAPIRestTemplate() {
        String rootURL = env.getProperty("api.v1.url.upload");
        RestTemplate restClient = new RestTemplate();
        DefaultUriTemplateHandler uriTemplateHandler = new DefaultUriTemplateHandler();
        uriTemplateHandler.setBaseUrl(rootURL);
        restClient.setUriTemplateHandler(uriTemplateHandler);

        return restClient;
    }
}
