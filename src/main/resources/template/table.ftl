<form class="form-horizontal" enctype="multipart/form-data">
    <fieldset>
        <div class="row">
            <legend class="center" id="txtLegend">${nameDanhSach}</legend>
            <table class="table table-bordered" linkthemmoi="" linkchitiet="" id="${nameDanhSach}" data-pagination="true" data-toggle="table" data-side-pagination="server" data-mobile-responsive="true" data-ajax="ajaxRequest" data-id-field="_id">
                <thead class="thead-light">
                <tr>
                    <th data-formatter="formatterRowIndex" data-field="stt" class="stt">STT</th>
                    ${listNameCollums}
                    <th data-formatter="formatterButtons" data-field="thaoTac" class="thaoTac">Thao tác</th>
                </tr>
                </thead>
                <tbody id="bodycongchuc">
                </tbody>
            </table>
        </div>
    </fieldset>
</form>