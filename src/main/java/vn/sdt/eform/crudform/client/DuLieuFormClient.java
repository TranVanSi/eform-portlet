package vn.sdt.eform.crudform.client;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;
import vn.sdt.eform.cauhinheform.dto.ResponseDTO;
import vn.sdt.eform.crudform.dto.DuLieuDTO;
import vn.sdt.eform.utils.Constants;

import java.io.File;
import java.util.Collections;
import java.util.List;

@Service
@RequiredArgsConstructor
public class DuLieuFormClient {
    private final String path = "/api/v1/dulieuform";

    @Autowired
    @Qualifier("eformRest")
    public RestTemplate restTemplate;

    public void saveDuLieuForm(String maForm, DuLieuDTO duLieuDTO) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON_UTF8);
        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(path)
                .queryParam("maForm", maForm);

        HttpEntity<DuLieuDTO> entity = new HttpEntity<>(duLieuDTO, headers);
        try {
            ResponseEntity<ResponseDTO<DuLieuDTO>> responseEntity = restTemplate.exchange(
                    builder.toUriString(), HttpMethod.POST, entity, new ParameterizedTypeReference<ResponseDTO<DuLieuDTO>>() {
                    });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public List<Object> getDanhSachByMaForm(String maForm) {
        String restUrl = path + "/findByMaForm";
        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(restUrl)
                .queryParam("maForm", maForm);
        try {
            ResponseEntity<ResponseDTO<List<Object>>> responseEntity = restTemplate.exchange(
                    builder.toUriString(), HttpMethod.GET, null, new ParameterizedTypeReference<ResponseDTO<List<Object>>>() {
                    });

            if (responseEntity.getBody().getStatus() == Constants.RESPONE.SUCCESS) {
                return responseEntity.getBody().getData();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return Collections.emptyList();
    }

    public Object findByMaFormAndId(String maForm, String objectId) {
        String restUrl = path + "/" + objectId;
        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(restUrl)
                .queryParam("maForm", maForm);
        try {
            ResponseEntity<ResponseDTO<Object>> responseEntity = restTemplate.exchange(
                    builder.toUriString(), HttpMethod.GET, null, new ParameterizedTypeReference<ResponseDTO<Object>>() {
                    });

            if (responseEntity.getBody().getStatus() == Constants.RESPONE.SUCCESS) {
                return responseEntity.getBody().getData();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    public boolean deleteDuLieuForm(String maForm, String objectId) {
        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(path + "/delete")
                .queryParam("maForm", maForm)
                .queryParam("objectId", objectId);
        HttpEntity<?> response = restTemplate.exchange(builder.toUriString(), HttpMethod.DELETE, null, String.class, new Object[0]);

        return ((ResponseEntity<?>) response).getStatusCode().is2xxSuccessful();
    }

    public List<Object> searchDuLieu(String maForm, String listTruongSearch, String listValueSearch) {
        String restUrl = path + "/search";
        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(restUrl)
                .queryParam("maForm", maForm)
                .queryParam("listTruongSearch", listTruongSearch)
                .queryParam("listValueSearch", listValueSearch);
        try {
            ResponseEntity<ResponseDTO<List<Object>>> responseEntity = restTemplate.exchange(
                    builder.toUriString(), HttpMethod.GET, null, new ParameterizedTypeReference<ResponseDTO<List<Object>>>() {
                    });

            if (responseEntity.getBody().getStatus() == Constants.RESPONE.SUCCESS) {
                return responseEntity.getBody().getData();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return Collections.emptyList();
    }

    public ResponseDTO filter(String maForm, String listTruongSearch, String listValueSearch, int offset, int size) {
        String restUrl = path + "/filter";
        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(restUrl)
                .queryParam("maForm", maForm)
                .queryParam("listTruongSearch", listTruongSearch)
                .queryParam("listValueSearch", listValueSearch)
                .queryParam("offset", offset)
                .queryParam("size", size);
        try {
            ResponseEntity<ResponseDTO> responseEntity = restTemplate.exchange(
                    builder.toUriString(), HttpMethod.GET, null, new ParameterizedTypeReference<ResponseDTO>() {
                    });

            if (responseEntity.getBody().getStatus() == Constants.RESPONE.SUCCESS) {
                return responseEntity.getBody();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return new ResponseDTO();
    }

    public ResponseDTO getUrlCauHinh(String maForm, String tenTable) {
        String restUrl = path + "/getUrlCauHinh";
        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(restUrl)
                .queryParam("maForm", maForm)
                .queryParam("tenTable", tenTable);
        try {
            ResponseEntity<ResponseDTO> responseEntity = restTemplate.exchange(
                    builder.toUriString(), HttpMethod.GET, null, new ParameterizedTypeReference<ResponseDTO>() {
                    });
            if (responseEntity.getBody().getStatus() == Constants.RESPONE.SUCCESS) {
                return responseEntity.getBody();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return new ResponseDTO();
    }

    public ResponseDTO uploadFileEform(File file, String maForm, String listKeyId, String listKeyMergeId) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.MULTIPART_FORM_DATA);

        MultiValueMap<String, Object> map = new LinkedMultiValueMap<>();
        map.add("file",  new FileSystemResource(file));
        map.add("maForm", maForm);
        map.add("listKeyId",  listKeyId);
        map.add("listKeyMergeId",  listKeyMergeId);

        HttpEntity<MultiValueMap<String, Object>> requestEntity = new HttpEntity<>(map, headers);
        //Utils.disableSslVerification();
        ResponseEntity<ResponseDTO> response = restTemplate.postForEntity(path + "/importFileDuLieu", requestEntity, ResponseDTO.class);

        return response.getBody();
    }

    public String findOne(String maForm, String listTruongSearch, String listValueSearch) {
        String restUrl = path + "/findone";
        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(restUrl)
                .queryParam("maForm", maForm)
                .queryParam("listTruongSearch", listTruongSearch)
                .queryParam("listValueSearch", listValueSearch);
        try {
            ResponseEntity<ResponseDTO<String>> responseEntity = restTemplate.exchange(
                    builder.toUriString(), HttpMethod.GET, null, new ParameterizedTypeReference<ResponseDTO<String>>() {
                    });

            if (responseEntity.getBody().getStatus() == Constants.RESPONE.SUCCESS) {
                return responseEntity.getBody().getData();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    public List<Object> getDataByPath(String pathSearch) {
        String restUrl = path + pathSearch;
        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(restUrl);
        try {
            ResponseEntity<ResponseDTO<List<Object>>> responseEntity = restTemplate.exchange(
                    builder.toUriString(), HttpMethod.GET, null, new ParameterizedTypeReference<ResponseDTO<List<Object>>>() {
                    });

            if (responseEntity.getBody().getStatus() == Constants.RESPONE.SUCCESS) {
                return responseEntity.getBody().getData();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return Collections.emptyList();
    }
}
