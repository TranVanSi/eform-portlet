package vn.sdt.eform.crudform.services;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.liferay.portal.kernel.upload.UploadPortletRequest;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.Validator;
import lombok.RequiredArgsConstructor;
import org.apache.commons.codec.binary.Base64;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;
import vn.sdt.eform.cauhinheform.client.CRUDServiceClient;
import vn.sdt.eform.cauhinheform.dto.RequestDTO;
import vn.sdt.eform.cauhinheform.dto.ResponseDTO;
import vn.sdt.eform.cauhinheform.dto.ResultDTO;
import vn.sdt.eform.cauhinheform.dto.ServiceDataDTO;
import vn.sdt.eform.crudform.client.DuLieuFormClient;
import vn.sdt.eform.crudform.client.FileUploadClient;
import vn.sdt.eform.crudform.dto.DuLieuDTO;
import vn.sdt.eform.utils.Authenticate;
import vn.sdt.eform.utils.Constants;
import vn.sdt.eform.utils.EformUtils;

import javax.portlet.ResourceRequest;
import java.io.File;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Service
@RequiredArgsConstructor
public class DuLieuFormService {
    @Autowired
    @Qualifier("objectMapper")
    public ObjectMapper objectMapper;
    private final DuLieuFormClient duLieuFormClient;
    private final CRUDServiceClient crudServiceClient;
    private final FileUploadClient fileUploadClient;

    public void saveDuLieuEform(String maForm, DuLieuDTO duLieuDTO) {
        duLieuFormClient.saveDuLieuForm(maForm, duLieuDTO);
    }

    public List<Object> getDanhSachByMaForm(String maForm) {
        return duLieuFormClient.getDanhSachByMaForm(maForm);
    }

    public void luuDuLieu(String maForm, String duLieu, String objectId) {
        DuLieuDTO duLieuDTO = new DuLieuDTO();
        if (Validator.isNotNull(objectId)){
            duLieuDTO.setId(objectId);
        }
        duLieuDTO.setDuLieu(duLieu);
        saveDuLieuEform(maForm, duLieuDTO);
    }

    public Object getDuLieuByMaFormAndObjectId(String maForm, String objectId) {
        return duLieuFormClient.findByMaFormAndId(maForm, objectId);
    }

    public void deleteDuLieu(String maForm, String objectId) {
        duLieuFormClient.deleteDuLieuForm(maForm, objectId);
    }

    public List<Object> searchDuLieu(String maForm, String listTruongSearch, String listValueSearch) {
        return duLieuFormClient.searchDuLieu(maForm, listTruongSearch, listValueSearch);
    }
    public ResponseDTO filter(String maForm, String listTruongSearch, String listValueSearch, int offset, int size) {
        return duLieuFormClient.filter(maForm, listTruongSearch, listValueSearch,offset, size);
    }

    public ResponseDTO getUrlCauHinh(String maForm, String tenTable) {
        return duLieuFormClient.getUrlCauHinh(maForm, tenTable);
    }

    private ResponseDTO getDataRespone(JSONObject data) throws Exception{
        ResponseDTO responseDTO = new ResponseDTO();
        if (data.has("objects")){
            responseDTO.setStatus(Constants.RESPONE.SUCCESS);
            responseDTO.setData(EformUtils.getListDataFromResponseService(data));
        }
        if (data.has("data")) {
            JSONArray ob = data.getJSONArray("data");
            responseDTO.setData(ob);
        }
        return responseDTO;
    }

    public String getAccessToken(JsonNode token, int loaiToken) throws Exception {
        String result = "";
        RestTemplate restTemplate = new RestTemplate();
        Authenticate.disableSslVerification();
        HttpHeaders headers = new HttpHeaders();
        MultiValueMap<String, String> map = new LinkedMultiValueMap<String, String>();
        String uri = "";
        if (token.has("url")) {
            uri = token.get("url").toString().substring(1, token.get("url").toString().length()-1);
        }
        HttpMethod method = HttpMethod.POST;

        if (token.has("method")) {
            if (token.get("method").toString().equals(Constants.METHOD.GET)) {
                method = HttpMethod.GET;
            }
        }
        if (loaiToken == Constants.LOAI_AUTHEN.BEARER) {
            if (token.has("params")) {
                JSONObject jsonObjectPr = new JSONObject(objectMapper.writeValueAsString(token.get("params")));
                if (jsonObjectPr.has("Content-Type")) {
                    headers.set("Content-Type", jsonObjectPr.getString("Content-Type"));
                }
                if (jsonObjectPr.has("client_id") && jsonObjectPr.has("client_secret") && jsonObjectPr.has("grant_type")) {
                    map.add("client_id", jsonObjectPr.getString("client_id"));
                    map.add("client_secret", jsonObjectPr.getString("client_secret"));
                    map.add("grant_type", jsonObjectPr.getString("grant_type"));
                }
            }

            HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<MultiValueMap<String, String>>(map, headers);
            UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(uri);
            HttpEntity<String> response = restTemplate.exchange(builder.toUriString(), method, request, String.class);
            org.json.JSONObject obj = new JSONObject(response.getBody());
            Object objNew = obj.get("access_token");
            result = objNew.toString();
        }
        if (loaiToken == Constants.LOAI_AUTHEN.BASIC) {
            if (token.has("username") && token.has("password")) {
                String auth = token.get("username") + ":" + token.get("password");
                byte[] encodedAuth = Base64.encodeBase64(
                        auth.getBytes(Charset.forName("US-ASCII")));
                String authHeader = "Basic " + new String(encodedAuth);
                headers.set("Authorization", authHeader);

                HttpEntity<?> request = new HttpEntity<>(headers);
                UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(uri);
                HttpEntity<String> response = restTemplate.exchange(builder.toUriString(), method, request, String.class);

                return response.getBody();
            }
        }
        return result;
    }

    public ResponseDTO importFileData(File file, String maForm, String listKeyId, String listKeyMergeId){
        return duLieuFormClient.uploadFileEform(file, maForm, listKeyId, listKeyMergeId);
    }

    public String findOne(String maForm, String listTruong, String listSearch) {
        return duLieuFormClient.findOne(maForm, listTruong, listSearch);
    }

    public String getDataByKeyword(String url, String keyWord, String _key, String _value) throws JsonProcessingException {
        List<Object> listDuLieu = duLieuFormClient.getDataByPath(url + keyWord);
        Map<Object, Object> map = listDuLieu.stream().collect(Collectors.toMap(p -> new JSONObject(p.toString()).get(_key), p -> new JSONObject(p.toString()).get(_value)));
        return objectMapper.writeValueAsString(map);
    }

    public ResponseDTO makeAndSendHttpRequest(ServiceDataDTO serviceDataDTO, JSONObject jsonObject, int loaiForm, int offset, int size, String query) throws Exception {
        ResponseDTO res = new ResponseDTO();
        String dataJSon = serviceDataDTO.getDataJson();
        objectMapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
        ResultDTO resultDTO = objectMapper.readValue(dataJSon, new TypeReference<ResultDTO>() {});
        if (loaiForm == Constants.LOAI_FORM.CREATE_SERVICE) {
            RequestDTO requestDTO = objectMapper.readValue(objectMapper.writeValueAsString(resultDTO.getItem().get(0).getRequest()), new TypeReference<RequestDTO>() {
            });
            JSONObject result = EformUtils.getBodyData(objectMapper.readValue(requestDTO.getBody().get("raw").toString(), JSONObject.class), jsonObject);
            sendHttpRequest(serviceDataDTO, requestDTO, result, new JSONObject(), 0, 0, query);
        }
        if (loaiForm == Constants.LOAI_FORM.DANHSACH_SERVICE){
            RequestDTO requestDTO = objectMapper.readValue(objectMapper.writeValueAsString(resultDTO.getItem().get(0).getRequest()), new TypeReference<RequestDTO>() {
            });
            JsonNode jsonBody = resultDTO.getItem().get(0).getResponse();
            JSONObject item = new JSONObject(objectMapper.writeValueAsString(jsonBody.get(0)));
            res = sendHttpRequest(serviceDataDTO, requestDTO, new JSONObject(), item, offset, size ,query);
        }

        return res;
    }

    public ResponseDTO sendHttpRequest(ServiceDataDTO serviceDataDTO ,RequestDTO requestDTO, JSONObject object, JSONObject respone, int offset, int size, String query) throws Exception {
        RestTemplate restTemplate = new RestTemplate();

        Authenticate.disableSslVerification();
        HttpHeaders headers = new HttpHeaders();
        headers.set("Accept", "application/json");
        if (Validator.isNotNull(requestDTO.getAuth())) {
            if (Validator.isNotNull(requestDTO.getAuth().get("type"))) {
                if (requestDTO.getAuth().get("type").toString().substring(1,requestDTO.getAuth().get("type").toString().length()-1).equals(Constants.AUTHEN.BEARER)) {
                    String token = serviceDataDTO.getToken();
                    headers.set(HttpHeaders.AUTHORIZATION, "Bearer " + token);
                }
            }
        }
        if (object.toString().length() > 2) {
            HttpEntity<String> request = new HttpEntity<>(object.toString(), headers);
            UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(requestDTO.getUrl().get("raw").toString().substring(1, requestDTO.getUrl().get("raw").toString().length() - 1));
            HttpMethod method = HttpMethod.POST;
            if (requestDTO.getMethod().equals(Constants.METHOD.GET)) {
                method = HttpMethod.GET;
            }
            if (requestDTO.getMethod().equals(Constants.METHOD.DELETE)) {
                method = HttpMethod.DELETE;
            }
            try {
                HttpEntity<String> response = restTemplate.exchange(builder.toUriString(), method, request, String.class);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (respone.toString().length() > 2) {
            HttpEntity<String> request = new HttpEntity<>(headers);
            String uri = "";
            if (requestDTO.getUrl().get("raw").toString().indexOf("offset")> -1){
                uri = requestDTO.getUrl().get("raw").toString().substring(1, requestDTO.getUrl().get("raw").toString().length()-Constants.CREATE_COLLECTION_TABLE.PATH_PHAN_TRANG.length());
            }else{
                uri = requestDTO.getUrl().get("raw").toString().substring(1, requestDTO.getUrl().get("raw").toString().length() - 1);
            }
            UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(uri)
                    .queryParam("offset", offset)
                    .queryParam("limit", size);
            String url = builder.toUriString().replace("_queryParam", query);

            HttpMethod method = HttpMethod.POST;
            if (requestDTO.getMethod().equals(Constants.METHOD.GET)) {
                method = HttpMethod.GET;
            }
            if (requestDTO.getMethod().equals(Constants.METHOD.DELETE)) {
                method = HttpMethod.DELETE;
            }
            try {
                ResponseEntity<String> response = restTemplate.exchange(url, method, request, String.class);
                JSONObject js = new JSONObject(response.getBody());
                return getDataRespone(js);
            } catch (Exception e) {
                if (e.getMessage().equals(Constants.ERROR_AUTHEN)){
                    String tokens = getAccessToken(requestDTO.getToken(), Constants.LOAI_AUTHEN.BEARER);
                    serviceDataDTO.setToken(tokens);
                    crudServiceClient.saveCauHinhCRUDService(serviceDataDTO);

                    headers.set("Accept", "application/json");
                    headers.set(HttpHeaders.AUTHORIZATION, "Bearer " + tokens);
                    request = new HttpEntity<>(headers);
                    ResponseEntity<String> response = restTemplate.exchange(url, method, request, String.class);
                    JSONObject js = new JSONObject(response.getBody());
                    return getDataRespone(js);
                }
            }
        }

        return null;

    }

    public List<String> uploadInfo(UploadPortletRequest uploadRequest, ResourceRequest resourceRequest) throws Exception {
        String typeUpload = ParamUtil.getString(resourceRequest, "typeUpload");
        File[] file = uploadRequest.getFiles(typeUpload);
        List<String> listUrl = new ArrayList<>();
        for (File item : file){
            String lienKet = fileUploadClient.saveFile2Server(item);
            listUrl.add(lienKet);
        }

        return listUrl;
    }
}
