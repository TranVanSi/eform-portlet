package vn.sdt.eform.utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

public class Utils {
    public static JSONArray objectToJSONArray(Object object){
        Object json = null;
        JSONArray jsonArray = null;
        try {
            json = new JSONTokener(object.toString()).nextValue();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if (json instanceof JSONArray) {
            jsonArray = (JSONArray) json;
        }
        return jsonArray;
    }

    public static List<String> getListIdHoSo(JSONObject object){
        object.remove("_id");
        List<String> listResult = new ArrayList<>();
        for (Iterator<String> item = object.keys(); item.hasNext(); ) {
            String result = item.next();
            if (isJSONValid(object.get(result).toString()) == Constants.CHECK_DATA.JSON_OBJECT){
                JSONObject js = object.getJSONObject(result);
                for (Iterator<String> items = js.keys(); items.hasNext(); ) {
                    String results = result+"["+items.next()+"]";
                    listResult.add(results);
                }
            }
            if (isJSONValid(object.get(result).toString()) == Constants.CHECK_DATA.JSON_ARRAY){
                JSONArray jsArr = object.getJSONArray(result);
                JSONObject js = jsArr.getJSONObject(0);
                for (Iterator<String> items = js.keys(); items.hasNext(); ) {
                    String results = result+"[0]["+items.next()+"]";
                    listResult.add(results);
                }
            }else {
                listResult.add(result);
            }
        }
        return listResult;
    }

    public static int isJSONValid(String sTr) {
        try {
            new JSONObject(sTr);
            return Constants.CHECK_DATA.JSON_OBJECT;
        } catch (JSONException ex) {
            try {
                new JSONArray(sTr);
                return Constants.CHECK_DATA.JSON_ARRAY;
            } catch (JSONException ex1) {
                return Constants.CHECK_DATA.STRING;
            }
        }
    }

    public static String convertDateToString(Date date) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        try {
            return sdf.format(date);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
