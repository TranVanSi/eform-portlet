var bien;
$(document).ready(function () {
    var obj = $("#objResult").val();
    bien = obj;
    if (typeof obj != 'undefined' && obj != ""){
        $("#objectId").val(JSON.parse(obj)["_id"]["$oid"]);
        $('form.form-horizontal fieldset div.row').find('label').each(function () {
            var id = $(this).attr("id");
            if (typeof id != 'undefined' && id != null){
                var dataMapping = JSON.parse(obj)["dataMapping"];
                var text =  $("#"+id).text();
                var label = JSON.parse(obj)[id];
                if (typeof dataMapping !== 'undefined' && JSON.parse(dataMapping).hasOwnProperty(id)) {
                    label = JSON.parse(dataMapping)[id];
                }
                var result = text + ": ";
                $("#"+id).text(result).parent().append("<span>" + label + "</span>");
            }
        });
    }

})