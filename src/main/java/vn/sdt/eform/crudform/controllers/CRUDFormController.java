package vn.sdt.eform.crudform.controllers;

import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.servlet.SessionMessages;
import com.liferay.portal.kernel.upload.UploadPortletRequest;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PortalUtil;
import com.liferay.portal.kernel.util.Validator;
import freemarker.template.Configuration;
import freemarker.template.DefaultObjectWrapper;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import lombok.RequiredArgsConstructor;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;
import vn.sdt.eform.cauhinheform.dto.ResponseDTO;
import vn.sdt.eform.cauhinheform.dto.ServiceDataDTO;
import vn.sdt.eform.cauhinheform.dto.CauHinhEformDTO;
import vn.sdt.eform.cauhinheform.services.CauHinhEformService;
import vn.sdt.eform.crudform.dto.DuLieuDTO;
import vn.sdt.eform.crudform.services.DuLieuFormService;
import vn.sdt.eform.utils.Constants;
import vn.sdt.eform.utils.Utils;

import javax.portlet.*;
import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Controller
@RequestMapping("VIEW")
@RequiredArgsConstructor
@PropertySource("classpath:config.properties")
public class CRUDFormController {
    private final CauHinhEformService cauHinhEformService;
    private final DuLieuFormService duLieuFormService;
    @Value("${TINH_THANH_MAC_DINH_MA}")
    private String TINH_THANH_MAC_DINH_MA;

    @RenderMapping
    public String view(RenderRequest request, Model model) throws Exception {
        PortletPreferences preferences = request.getPreferences();
        String maForm = preferences.getValue("maForm", "");
        String isImport = preferences.getValue("isImport", "");
        String objectId = ParamUtil.getString(request, "objectId");
        String maFormDanhSach = ParamUtil.getString(request, "maFormDanhSach");

        int loai = 0;
        CauHinhEformDTO cauHinhEformDTO = new CauHinhEformDTO();
        if (maForm != null) {
            cauHinhEformDTO = cauHinhEformService.findByMa(maForm);
        }

        if (Validator.isNotNull(maFormDanhSach)) {
            cauHinhEformDTO = cauHinhEformService.findByMa(maFormDanhSach);
        }
        if (Validator.isNotNull(cauHinhEformDTO)) {
            loai = cauHinhEformDTO.getLoai();
            model.addAttribute("cauHinhEformDTO", cauHinhEformDTO);
        }

        HttpServletRequest renderRequest = PortalUtil.getOriginalServletRequest(PortalUtil
                .getHttpServletRequest(request));
        String idEdit = ParamUtil.getString(renderRequest, "idEdit");
        String maFormEdit = ParamUtil.getString(renderRequest, "maFormEdit");
        String idChiTiet = ParamUtil.getString(renderRequest, "idChiTiet");
        String maFormChiTiet = ParamUtil.getString(renderRequest, "maFormChiTiet");
        String loaiEdit = ParamUtil.getString(renderRequest, "loai");
        String loaiChiTiet = ParamUtil.getString(renderRequest, "loaiChiTiet");
        long xemChiTiet = ParamUtil.getLong(renderRequest, "xemChiTiet");
        String maFormPage = ParamUtil.getString(renderRequest, "maFormPage");
        String idDiChuyen = ParamUtil.getString(renderRequest, "idDiChuyen");
        String maFormDiChuyen = ParamUtil.getString(renderRequest, "maFormDiChuyen");

        if (Validator.isNotNull(idEdit) && Validator.isNotNull(maFormEdit) && Validator.isNotNull(loaiEdit)) {
            Object obj = duLieuFormService.getDuLieuByMaFormAndObjectId(maFormEdit, idEdit);

            if (Validator.isNotNull(obj)) {
                model.addAttribute("obj", obj);
                loai = Integer.parseInt(loaiEdit);
            }
        }

        if (Validator.isNotNull(idChiTiet) && Validator.isNotNull(maFormChiTiet) && Validator.isNotNull(loaiChiTiet)) {
            Object obj = duLieuFormService.getDuLieuByMaFormAndObjectId(maFormChiTiet, idChiTiet);

            if (Validator.isNotNull(obj)) {
                model.addAttribute("obj", obj);
                loai = Integer.parseInt(loaiChiTiet);
            }
        }

        if (Validator.isNotNull(idDiChuyen) && Validator.isNotNull(maFormDiChuyen)){
            Object obj = duLieuFormService.getDuLieuByMaFormAndObjectId(maFormDiChuyen, idDiChuyen);
            if (Validator.isNotNull(obj)) {
                model.addAttribute("obj", obj);
                model.addAttribute("addDiChuyen", "addDiChuyen");
                model.addAttribute("maFormDiChuyen", maFormDiChuyen);
            }
        }
        if (xemChiTiet > 0){
            model.addAttribute("isXemChiTiet", "isXemChiTiet");
        }
        if (!TINH_THANH_MAC_DINH_MA.equals("0")){
            model.addAttribute("tinhThanhSelected", TINH_THANH_MAC_DINH_MA);
        }
        model.addAttribute("maForm", maForm);
        model.addAttribute("isImport", isImport);
        model.addAttribute("maFormPage", maFormPage);
        model.addAttribute("loai", loai);

        return "view";
    }

    @ActionMapping(params = "action=themMoi")
    public void themMoi(ActionRequest actionRequest, ActionResponse actionResponse, @RequestParam(value = "listId", defaultValue = "") String listId,
                        @RequestParam(value = "maForm", defaultValue = "") String maForm,
                        @RequestParam(value = "objectId", defaultValue = "") String objectId,
                        @RequestParam(value = "dataMapping", defaultValue = "") String dataMapping,
                        @RequestParam(value = "addDiChuyen", defaultValue = "") String addDiChuyen,
                        @RequestParam(value = "maFormDiChuyen", defaultValue = "") String maFormDiChuyen,
                        @RequestParam(value = "objResult", defaultValue = "") String objResult,
                        @RequestParam(value = "maFormPage", defaultValue = "") String maFormPage) throws Exception {
        String email = "";
        User user = PortalUtil.getUser(actionRequest);
        if (Validator.isNotNull(user)){
            email = user.getEmailAddress();
        }
        if (Validator.isNotNull(listId) && Validator.isNotNull(maForm)) {
            JSONObject object = new JSONObject();
            if (Validator.isNotNull(addDiChuyen)){
                object = new JSONObject(objResult);
                object.remove("_id");
            }else{
                object.put("dataMapping", new JSONObject(dataMapping));
                object.put("ngayTao", Utils.convertDateToString(new Date()));
                object.put("nguoiTao", email);
                object.put("nguoiSua", email);
                object.put("ngaySua", Utils.convertDateToString(new Date()));
            }

            String[] ids = listId.split(",");
            for (String item : ids) {
                String value = ParamUtil.getString(actionRequest, item);
                int checkData = Utils.isJSONValid(value);
                if (checkData == Constants.CHECK_DATA.JSON_OBJECT){
                    object.put(item, new JSONObject(value));
                }
                if (checkData == Constants.CHECK_DATA.JSON_ARRAY){
                    object.put(item, new JSONArray(value));
                }
                if (checkData == Constants.CHECK_DATA.STRING){
                    object.put(item, value);
                }
            }

            CauHinhEformDTO cauHinhEformDTO = new CauHinhEformDTO();
            if (Validator.isNotNull(maFormDiChuyen)){
                cauHinhEformDTO = cauHinhEformService.findByMa(maFormDiChuyen);
            }else{
                cauHinhEformDTO = cauHinhEformService.findByMa(maForm);
            }

            if (Validator.isNotNull(cauHinhEformDTO)){
                ServiceDataDTO serviceDataDTO = cauHinhEformService.findByCauHinhId(cauHinhEformDTO.getId());
                if (Validator.isNotNull(serviceDataDTO)){
                    duLieuFormService.makeAndSendHttpRequest(serviceDataDTO, object, cauHinhEformDTO.getLoai(), 0,0, null);
                }else{
                    duLieuFormService.luuDuLieu(cauHinhEformDTO.getMaForm(), object.toString(), objectId);
                }
            }
            if (Validator.isNotNull(objectId)) {
                actionResponse.setRenderParameter("objectId", objectId);
            }
            if (Validator.isNotNull(maFormPage)) {
                actionResponse.setRenderParameter("maFormDanhSach", maFormPage);
            }
            SessionMessages.add(actionRequest, "success");

        }
    }

    @ActionMapping(params = "action=delete")
    public void xoa(SessionStatus sessionStatus, ActionRequest actionRequest, ActionResponse actionResponse,
                    @RequestParam(value = "maFormDelete", defaultValue = "") String maEform,
                    @RequestParam(value = "idDelete", defaultValue = "") String id,
                    @RequestParam(value = "maFormDS", defaultValue = "") String maFormDS) {

        duLieuFormService.deleteDuLieu(maEform, id);
        if (Validator.isNotNull(maFormDS)) {
            actionResponse.setRenderParameter("maFormDanhSach", maFormDS);
        }
        sessionStatus.setComplete();
        SessionMessages.add(actionRequest, "success");
    }

    @ActionMapping(params = "action=importFile")
    public void importFile(ActionRequest actionRequest, ActionResponse actionResponse,
                           SessionStatus sessionStatus, Model model,
                           @RequestParam(value = "maForm", defaultValue = "") String maForm,
                           @RequestParam(value = "listKeyId", defaultValue = "") String listKeyId,
                           @RequestParam(value = "listKeyMergeId", defaultValue = "") String listKeyMergeId) {
        UploadPortletRequest request = PortalUtil.getUploadPortletRequest(actionRequest);
        File file = request.getFile("fileImport");
        if (Validator.isNotNull(file) && file.length() > 0) {
            ResponseDTO responseDTO = duLieuFormService.importFileData(file, maForm, listKeyId, listKeyMergeId);
            model.addAttribute("thongBao", "Import thành công");
            SessionMessages.add(actionRequest, "form-success");
        }
        actionResponse.setRenderParameter("action", "");
    }

    @RenderMapping(params = "action=in")
    public String in(Model model, RenderRequest request) throws IOException, TemplateException {
        String id = ParamUtil.getString(request, "id");
        String objectId = ParamUtil.getString(request, "objectId");
        String maForm = ParamUtil.getString(request, "maForm");

        Object obj = duLieuFormService.getDuLieuByMaFormAndObjectId("TEMPLATE_PRINT", id);
        JSONObject jsonObject = new JSONObject(obj.toString());
        Map<String, Object> modelData = new HashMap<>();

        JSONObject duLieu = new JSONObject(duLieuFormService.getDuLieuByMaFormAndObjectId(maForm, objectId).toString());

        JSONObject dataMapping = null;
        if (duLieu.has("dataMapping")) {
            dataMapping = new JSONObject(duLieu.get("dataMapping"));
        }
        JSONObject finalDataMapping = dataMapping;
        duLieu.keySet().forEach(_key -> {
            if (finalDataMapping != null && finalDataMapping.has(_key)) {
                modelData.put(_key, finalDataMapping.get(_key));
            } else {
                modelData.put(_key, duLieu.get(_key));
            }
        });

        Configuration cfg = new Configuration();
        cfg.setObjectWrapper(new DefaultObjectWrapper());

        Template t = new Template("templateName", new StringReader(jsonObject.getString("html")), cfg);
        model.addAttribute("obj", FreeMarkerTemplateUtils.processTemplateIntoString(t, modelData));

        return "print";
    }
}
