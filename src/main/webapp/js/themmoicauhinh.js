var removeDuLieuUrl = $("#removeDuLieuUrl").val();
var checkValidateDuLieuUrl = $("#checkValidateDuLieuUrl").val();
var getTemplateMauUrl = $("#getTemplateMauUrl").val();
var actualBtn = document.getElementById('fileTaiLen');
var fileChosen = document.getElementById('file-chosen');
var urlCheckInfoUpload = $("#urlCheckInfoUpload").val();
actualBtn.addEventListener('change', function(){
    fileChosen.textContent = this.files[0].name
})

var getListIdCauHinhUrl = $("#getListIdCauHinhUrl").val();
var getDanhSachCauHinhUrl = $("#getDanhSachCauHinhUrl").val();
var getDanhSachEformUrl = $("#getDanhSachEformUrl").val();
var loadJS = $('#loadJS').val();
var collect = [];
var collectionHung;
var busyi = new busy_indicator(document.getElementById("busybox"),
    document.querySelector("#busybox div"));


$(document).ready(function () {
    changeTypeSelect();
    reload(loadJS);
    $('#type_input').change(function () {
        changeTypeSelect();
    });
    var loai = $("#loaiForm").val();
    getUpload(loai);
});

function getTemplateMau(ma) {
    $.ajax({
        type: 'GET'
        , url: getTemplateMauUrl
        , async: false
        , data: {
            maForm: ma
        }
        , success: function (data) {
            if (typeof  data != 'undefined') {
                var coll = decodeURI(data);
                if (coll.length > 0) {
                    var str = coll;
                    collect.push.apply(collect, JSON.parse(str))
                    collectionHung.models.splice(0);
                    collectionHung.add(JSON.parse(str));
                }
            }
        }
    });
}

function getUpload(value) {
    if (value == 4 || value == 5){
        $("#anHienUpLoad").prop("style","");
        $('#import_file').hide();
    } else if (value == 6) {
        $('#import_file').show();
        $("#anHienUpLoad").prop("style","display:none");
    } else {
        $("#anHienUpLoad").prop("style","display:none");
        $('#import_file').hide();
    }

}

$("#btnQuayLai").click(function () {
    var homeUrl = $("#homeUrl").val();
    window.location.href = homeUrl;
});

function editEform() {
    var coll = $('#collectionEdit').val();
    coll = decodeURI(coll);
    if (coll.length > 0) {
        var str = coll;
        collect.push.apply(collect, JSON.parse(str))
        collectionHung.models.splice(0);
        collectionHung.add(JSON.parse(str));

        $('#collectionEdit').val('');
    }
}

function removeElement(array, elem) {
    var index = array.indexOf(elem);
    if (index > -1) {
        array.splice(index, 1);
    }
}

function luuEform() {
    var tenEform = $('#tenEform').val();
    var maEform = $('#maEform').val();
    var id = $("#id").val();
    var trangThaiEform = $("input[name='trangThaiEform']:checked").val();

    var ischeck = true;
    var obj = document.getElementById("tenEform");
    var objMa = document.getElementById("maEform");

    if (tenEform == '') {
        obj.setAttribute("style", "background-color:rgb(254,243,244) !important;border: 1px solid red !important;");
        obj.setAttribute("title", "**Vui lòng nhập dữ liệu!");
        ischeck = false;
    } else {
        obj.setAttribute("style", "");
        obj.setAttribute("title", "");
    }

    if (maEform == '') {
        objMa.setAttribute("style", "background-color:rgb(254,243,244) !important;border: 1px solid red !important;");
        objMa.setAttribute("title", "**Vui lòng nhập dữ liệu!");
        ischeck = false;
    } else {
        objMa.setAttribute("style", "");
        objMa.setAttribute("title", "");
    }
    if (maEform != '') {
        $.ajax({
            type: 'GET'
            , url: checkValidateDuLieuUrl
            , async: false
            , data: {
                maForm: maEform,
                id: id
            }
            , success: function (data) {
                if (data == '1') {
                    objMa.setAttribute("style", "background-color:rgb(254,243,244) !important;border: 1px solid red !important;");
                    objMa.setAttribute("title", "**Mã đã tồn tại trong hệ thống!");
                    ischeck = false;
                }
            }
        });
    }
    if (ischeck) {

        $('[name="html"]').val($('#render').val().replace(/\n|\t/g, ' '));

        $('#tenForm').val($('#tenEform').val());
        $('#maForm').val($('#maEform').val());
        $('#loai').val($('#loaiForm').val());
        $('#trangThaiForm').val(trangThaiEform);

        var data = JSON.stringify(collectionHung.models).replace(/\n|\t/g, ' ');

        $('#collection').val(encodeURI(data));
        var listId = "";
        $('form#target fieldset').find('select, radio, checkbox, input, textarea').each(function () {
            var name = $(this).attr("name");
            listId += name + ",";
        });
        if (listId.length > 1) {
            listId = listId.substring(0, listId.length - 1);
        }
        $('#listId').val(listId);
        $('#formLuuEform').submit();
    } else {
        return false;
    }
}

var lienKetData = [];
var tableSearch = [];
var tableEform = [];
var danhSachDanhMuc = [];
var danhSachFormCauHinh = [];
function lienKetDaTaFunction() {
    lienKetData.push('{"value": "","label": "Không liên kết","selected": true}')
    lienKetData.push('{"value": "currentUserEmail","label": "Email đăng nhập","selected": false}')
    lienKetData.push('{"value": "currentUserCMND","label": "CMND đăng nhập","selected": false}')
    lienKetData.push('{"value": "currentUserHoTen","label": "Họ tên đăng nhập ","selected": false}')
    lienKetData.push('{"value": "currentUserSoDienThoai","label": "Số điện thoại đăng nhập","selected": false}')

    tableSearch.push('{"value": "","label": "---Chọn---","selected": true}')
    tableSearch.push('{"value": "dantoc","label": "Dân tộc","selected": false}')
    tableSearch.push('{"value": "donvihanhchinh","label": "Đơn vị hành chính","selected": false}')
    tableSearch.push('{"value": "coquan","label": "Cơ quan quản lý","selected": false}')

    danhSachDanhMuc.push('{"value": "","label": "Không sử dụng", "selected": true}')
    danhSachDanhMuc.push('{"value": "danToc","label": "Dân tộc", "selected": false}')
    danhSachDanhMuc.push('{"value": "quocGia","label": "Quốc gia", "selected": false}')
    danhSachDanhMuc.push('{"value": "tinhThanh","label": "Tỉnh/Thành Phố", "selected": false}')
    danhSachDanhMuc.push('{"value": "quanHuyen","label": "Quận/Huyện", "selected": false}')
    danhSachDanhMuc.push('{"value": "phuongXa","label": "Phường/Xã", "selected": false}')

    danhSachFormCauHinh.push('{"value": "","label": "---Chọn---","selected": true}')
    danhSachFormCauHinh.push('{"value": "tablecon","label": "Kiểu table con","selected": false}')

    tableEform.push('{"value": "","label": "---Chọn---","selected": true}')
}

function loadDanhSachCauHinhForm() {
    $.ajax({
        type: 'GET'
        , url: getDanhSachCauHinhUrl
        , async: false
        , success: function (data) {
            for (var i = 0; i < data.length; i++){
                danhSachFormCauHinh.push('{"value": "'+data[i].maForm+'","label": "'+data[i].tenForm+'", "selected": false}')
            }
        }
    });
}
function reload(src) {
    var isAdd = $('#isAdd').val();
    lienKetDaTaFunction();
    loadDanhSachCauHinhForm();
    loadDanhSachEform(1);
    loadDanhSachEform(5);
    $('#target').empty();
    $('#target fieldset').remove();
    $('#formtabs').empty();

    $.when(loadScript(src)).done(function (e) {
        var maForm = $('#maForm').val();
        if (maForm.length > 0) {
            busyi.show();
            setTimeout(function () {
                editEform();
                busyi.hide();
            }, 3000);
        }
    });
    if (isAdd == '1') {
        $('#maEform').attr('value', '');
        $('#tenEform').attr('value', '');
        $('#tenForm').attr('value', '');
        $('#maForm').attr('value', '');
        $('#id').attr('value', '');
    }
};

function loadScript(src) {
    $.getScript(src);
}

function changeTypeSelect() {
    var value = $('#type_input').val();

    $('[id^=validate]').each(function () {
        $(this).parent().parent().hide();

    });
    if (value == 'text') {
        $('[id^=validate_text]').each(function () {
            $(this).prop('style', '');
            $(this).parent().parent().prop('style', '');
        });
    } else if (value == 'date') {
        if (document.getElementById("validate_text_lien_ket_data") != null) {
            document.getElementById("validate_text_lien_ket_data").selectedIndex = "0";
        }
    } else if (value == 'search') {
        if (document.getElementById("validate_text_lien_ket_data") != null) {
            document.getElementById("validate_text_lien_ket_data").selectedIndex = "0";
        }
        $('[id^=validate_search]').each(function () {
            $(this).parent().parent().prop('style', '');
        })
    } else if (value == 'selectEform') {
        $('[id^=validate_eform]').each(function () {
            $(this).parent().parent().prop('style', '');
        });
        $('[id^=validate_label_eform]').each(function () {
            $(this).parent().parent().prop('style', '');
        });
        $('[id^=validate_key_eform]').each(function () {
            $(this).parent().parent().prop('style', '');
        })
    }
}

function changeCauHinh(value) {
    var xhtml='stt\n';
    $.ajax({
        type: 'GET'
        , url: getListIdCauHinhUrl
        , async: false
        , data: {
            maForm: value
        }
        , success: function (data) {
            if (typeof  data != 'undefined') {
                var result = data.split(",");
                for (var i = 0; i < result.length; i++) {
                    var rs = result[i];

                    xhtml += rs + "\n";
                }
                $("#values").html(xhtml.substring(0, xhtml.length - 1));
            }
        }
    });

};

function changeCauHinhLabel(value) {
    $("#idlabel").empty();
    var xhtml='<option value="">---Chọn---</option>';
    $.ajax({
        type: 'GET'
        , url: getListIdCauHinhUrl
        , async: false
        , data: {
            maForm: value
        }
        , success: function (data) {
            if (typeof  data != 'undefined') {
                var result = data.split(",");
                for (var i = 0; i < result.length; i++) {
                    var rs = result[i];
                    xhtml += '<option value="'+rs+'">'+rs+'</option>';
                }
                $("#idlabel").html(xhtml);
            }
        }
    });

}

$("#fileTaiLen").change(function () {
    var maForm = $("#maEform").val();
    checkDB(maForm);
    ajaxUpLoadForm();
});

function checkDB(maForm) {
    $.ajax({
        type: 'GET'
        , url: removeDuLieuUrl
        , async: false
        , data: {
            maForm: maForm
        }
        , success: function (data) {
        }
    });
}

function ajaxUpLoadForm() {

    var maForm = $("#maEform").val();
    var tenForm = $("#tenEform").val();
    var loai = $("#loaiForm").val();
    var file = $("#fileTaiLen")[0].files[0];
    var form_data = new FormData();
    form_data.append("fileTaiLen", file);
    form_data.append("maForm", maForm);
    form_data.append("tenForm", tenForm);
    form_data.append("loaiForm", loai);
    $.ajax({
        type: "POST",
        url: urlCheckInfoUpload,
        data: form_data,
        processData: false,
        contentType: false,
        cache: false,
        timeout: 1000000,
        success: function (data, textStatus, jqXHR) {
            if (typeof data != 'undefined' && data != ""){
                $("#cauHinhId").val(data);
                $("#formGetHtml").submit();
            }
        }
        ,
        error: function (jqXHR, textStatus, errorThrown) {
        }

    });
}

function loadDanhSachEform(loai) {
    $.ajax({
        type: 'GET'
        , url: getDanhSachEformUrl
        , data: {loai: loai}
        , async: false
        , success: function (data) {
            for (var i = 0; i < data.length; i++){
                tableEform.push('{"value": "'+data[i].maForm+'","label": "'+data[i].tenForm+'", "selected": false}')
            }
        }
    });
}


function enableBtn() {
    if ($('#fileImport').val().length > 0) {
        $('.btn-import').prop('disabled','');
    }
}

function changeCauHinhEform(value) {
    $("#validate_key_eform").empty();
    $("#validate_label_eform").empty();
    $("#filter_key_eform").empty();
    var xhtml='<option value="">---Chọn---</option>';
    $.ajax({
        type: 'GET'
        , url: getListIdCauHinhUrl
        , async: false
        , data: {
            maForm: value
        }
        , success: function (data) {
            if (typeof  data != 'undefined') {
                var result = data.split(",");
                for (var i = 0; i < result.length; i++) {
                    var rs = result[i];
                    xhtml += '<option value="'+rs+'">'+rs+'</option>';
                }
                $("#validate_key_eform").html(xhtml);
                $("#validate_label_eform").html(xhtml);
                $("#filter_key_eform").html(xhtml);
            }
        }
    });
}


