package vn.sdt.eform.cauhinheform.client;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.core.io.FileSystemResource;
import org.springframework.data.domain.Page;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;
import vn.sdt.eform.cauhinheform.dto.CauHinhEformDTO;
import vn.sdt.eform.cauhinheform.dto.ResponseDTO;
import vn.sdt.eform.config.RestPageImpl;
import vn.sdt.eform.utils.Constants;

import java.io.File;
import java.util.Collections;
import java.util.List;

@Service
@RequiredArgsConstructor
public class CauHinhEformClient {
    private final String path = "/api/v1/cauhinheform";

    @Autowired
    @Qualifier("eformRest")
    public RestTemplate restTemplate;

    public CauHinhEformDTO savecauHinhForm(CauHinhEformDTO cauHinhEform) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON_UTF8);
        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(path);

        HttpEntity<CauHinhEformDTO> entity = new HttpEntity<>(cauHinhEform, headers);
        try {
            ResponseEntity<ResponseDTO<CauHinhEformDTO>> responseEntity = restTemplate.exchange(
                    builder.toUriString(), HttpMethod.POST, entity, new ParameterizedTypeReference<ResponseDTO<CauHinhEformDTO>>() {
                    });
            if (responseEntity.getBody().getStatus() == Constants.RESPONE.SUCCESS) {
                return responseEntity.getBody().getData();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    public Page<CauHinhEformDTO> getDanhSachCauHinh(int offset, int limit, String search) {
        String restURL = path + "/search";
        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(restURL)
                .queryParam("offset", offset)
                .queryParam("limit", limit)
                .queryParam("search", search);
        try {
            ResponseEntity<ResponseDTO<RestPageImpl<CauHinhEformDTO>>> responseEntity = restTemplate.exchange(
                    builder.toUriString(), HttpMethod.GET, null, new ParameterizedTypeReference<ResponseDTO<RestPageImpl<CauHinhEformDTO>>>() {
                    });

            if (responseEntity.getBody().getStatus() == Constants.RESPONE.SUCCESS) {
                return responseEntity.getBody().getData();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    public CauHinhEformDTO findByMa(String maForm) {
        String restUrl = path + "/findByMa";
        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(restUrl)
                .queryParam("ma", maForm);
        try {
            ResponseEntity<ResponseDTO<CauHinhEformDTO>> responseEntity = restTemplate.exchange(
                    builder.toUriString(), HttpMethod.GET, null, new ParameterizedTypeReference<ResponseDTO<CauHinhEformDTO>>() {
                    });

            if (responseEntity.getBody().getStatus() == Constants.RESPONE.SUCCESS) {
                return responseEntity.getBody().getData();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    public boolean deleteCauHinhEform(String maEform) {
        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(path + "/delete").queryParam("ma", maEform);
        HttpEntity<?> response = restTemplate.exchange(builder.toUriString(), HttpMethod.DELETE, null, String.class, new Object[0]);

        return ((ResponseEntity<?>) response).getStatusCode().is2xxSuccessful();
    }

    public List<CauHinhEformDTO> getDanhSachCauHinh() {
        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(path);

        try {
            ResponseEntity<ResponseDTO<List<CauHinhEformDTO>>> responseEntity = restTemplate.exchange(
                    builder.toUriString(), HttpMethod.GET, null, new ParameterizedTypeReference<ResponseDTO<List<CauHinhEformDTO>>>() {
                    });

            if (responseEntity.getBody().getStatus() == Constants.RESPONE.SUCCESS) {
                return responseEntity.getBody().getData();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return Collections.emptyList();
    }

    public CauHinhEformDTO findById(String id) {
        String restUrl = path + "/"+ id;
        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(restUrl);
        try {
            ResponseEntity<ResponseDTO<CauHinhEformDTO>> responseEntity = restTemplate.exchange(
                    builder.toUriString(), HttpMethod.GET, null, new ParameterizedTypeReference<ResponseDTO<CauHinhEformDTO>>() {
                    });

            if (responseEntity.getBody().getStatus() == Constants.RESPONE.SUCCESS) {
                return responseEntity.getBody().getData();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    public ResponseDTO uploadFileEform(File file) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.MULTIPART_FORM_DATA);

        MultiValueMap<String, Object> map = new LinkedMultiValueMap<>();
        map.add("file",  new FileSystemResource(file));

        HttpEntity<MultiValueMap<String, Object>> requestEntity = new HttpEntity<>(map, headers);
        ResponseEntity<ResponseDTO> response = restTemplate.postForEntity(path + "/importFile", requestEntity, ResponseDTO.class);

        return response.getBody();
    }

    public List<CauHinhEformDTO> getDanhSachCauHinhByLoai(int loai) {
        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(path + "/loai/" + loai);

        try {
            ResponseEntity<ResponseDTO<List<CauHinhEformDTO>>> responseEntity = restTemplate.exchange(
                    builder.toUriString(), HttpMethod.GET, null, new ParameterizedTypeReference<ResponseDTO<List<CauHinhEformDTO>>>() {
                    });

            if (responseEntity.getBody().getStatus() == Constants.RESPONE.SUCCESS) {
                return responseEntity.getBody().getData();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return Collections.emptyList();
    }
}
