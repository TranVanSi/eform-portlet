package vn.sdt.eform.cauhinheform.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.liferay.portal.kernel.upload.UploadPortletRequest;
import com.liferay.portal.kernel.util.PortalUtil;
import com.liferay.portal.kernel.util.Validator;
import lombok.RequiredArgsConstructor;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.portlet.bind.annotation.ResourceMapping;
import vn.sdt.eform.cauhinheform.dto.ResponseDTO;
import vn.sdt.eform.cauhinheform.dto.ServiceDataDTO;
import vn.sdt.eform.cauhinheform.dto.CauHinhEformDTO;
import vn.sdt.eform.cauhinheform.services.CauHinhEformService;
import vn.sdt.eform.crudform.services.DuLieuFormService;
import vn.sdt.eform.utils.Constants;
import vn.sdt.eform.utils.Utils;

import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;
import java.io.File;
import java.io.IOException;
import java.util.List;

@Controller
@RequestMapping("VIEW")
@RequiredArgsConstructor
public class CauHinhEformAjaxController {
    private final CauHinhEformService cauHinhEformService;
    private final DuLieuFormService duLieuFormService;
    @Autowired
    @Qualifier("objectMapper")
    public ObjectMapper objectMapper;

    @ResourceMapping(value = "getDanhSachEform")
    public void getDanhSachEform(ResourceResponse response,
                                 @RequestParam(value = "offset", defaultValue = "0") int offset,
                                 @RequestParam(value = "limit", defaultValue = "10") int limit,
                                 @RequestParam(value = "search", defaultValue = "") String search) throws IOException {

        Page<CauHinhEformDTO> listEform = cauHinhEformService.getDanhSachCauHinh(offset, limit, search);

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("total", listEform.getTotalElements());
        jsonObject.put("rows", listEform.getContent());

        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write(jsonObject.toString());
    }

    @ResourceMapping(value = "getDanhSachCauHinh")
    public void getDanhSachEform(ResourceResponse response) throws IOException {

        List<CauHinhEformDTO> listCauHinh = cauHinhEformService.getDanhSach();

        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write(objectMapper.writeValueAsString(listCauHinh));
    }

    @ResourceMapping(value = "getListIdCauHinh")
    public void getListIdCauHinh(ResourceResponse response,
                                 @RequestParam(value = "maForm", defaultValue = "") String maForm) throws IOException {

        CauHinhEformDTO cauHinhEformDTO = cauHinhEformService.findByMa(maForm);
        if (Validator.isNotNull(cauHinhEformDTO)) {
            if (Validator.isNotNull(cauHinhEformDTO.getListId())) {
                response.getWriter().write(cauHinhEformDTO.getListId());
            }
        }
    }

    @ResourceMapping(value = "getInfoUpload")
    public void getInfoImage(ResourceResponse response, ResourceRequest request,
                             @RequestParam(value = "maForm", defaultValue = "") String maForm,
                             @RequestParam(value = "tenForm", defaultValue = "") String tenForm,
                             @RequestParam(value = "loaiForm", defaultValue = "") String loaiForm) throws Exception {

        UploadPortletRequest uploadRequest = PortalUtil.getUploadPortletRequest(request);
        File file = uploadRequest.getFile("fileTaiLen");
        response.getWriter().write(cauHinhEformService.getInfoUpload(file, maForm, tenForm, loaiForm));
    }

    @ResourceMapping(value = "checkValidateDuLieu")
    public void checkValidateDuLieu(ResourceResponse response,
                             @RequestParam(value = "id", defaultValue = "") String cauHinhId,
                                    @RequestParam(value = "maForm", defaultValue = "") String maForm) throws Exception {
        String result = "0";
        if (Validator.isNotNull(cauHinhId)) {
            CauHinhEformDTO cauHinhEformDTO = cauHinhEformService.findById(cauHinhId);
            CauHinhEformDTO cauHinhEformUpdate  = cauHinhEformService.findByMa(maForm);
            if (Validator.isNotNull(cauHinhEformUpdate) && !cauHinhEformDTO.getMaForm().equals(cauHinhEformUpdate.getMaForm())) {
                result = "1";
            }
        }
        response.getWriter().write(result);
    }

    @ResourceMapping(value = "removeDuLieu")
    public void removeDuLieu(@RequestParam(value = "maForm", defaultValue = "") String maForm) {
        CauHinhEformDTO cauHinhEformDTO = cauHinhEformService.findByMa(maForm);
        if (Validator.isNotNull(cauHinhEformDTO)){
           String id = cauHinhEformDTO.getId();
            ServiceDataDTO serviceDataDTO = cauHinhEformService.findByCauHinhId(id);
            if (Validator.isNotNull(serviceDataDTO)) {
                cauHinhEformService.deleteCauHinhCRUDService(id);
            }
            cauHinhEformService.deleteCauHinhEform(maForm);
        }
    }

    @ResourceMapping(value = "getDanhSachCauHinhByLoai")
    public void getDanhSachCauHinhByLoai(ResourceResponse response,
                                         @RequestParam(value = "loai", defaultValue = "0") int loai) throws IOException {

        List<CauHinhEformDTO> listCauHinh = cauHinhEformService.getDanhSachCauHinhByLoai(loai);

        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write(objectMapper.writeValueAsString(listCauHinh));
    }

    @ResourceMapping(value = "getDanhSachTemplate")
    public void searchTemplatePrint(ResourceRequest request, ResourceResponse response,
                       @RequestParam(value = "maForm", defaultValue = "") String maForm,
                       @RequestParam(value = "offset", defaultValue = "0") int offset,
                       @RequestParam(value = "limit", defaultValue = "10") int limit) throws Exception {
        ResponseDTO duLieu = new ResponseDTO();
        duLieu = duLieuFormService.filter(Constants.MA_FORM_TEMPLATE_PRINT, "maForm", maForm, offset, limit);
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("total", duLieu.getTotalItems());
        jsonObject.put("rows", Utils.objectToJSONArray(duLieu.getData()));
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write(jsonObject.toString());
    }

    @ResourceMapping(value = "getTemplateMau")
    public void getTemplateMau(ResourceResponse response,
                                 @RequestParam(value = "maForm", defaultValue = "") String maForm) throws IOException {

        CauHinhEformDTO cauHinhEformDTO = cauHinhEformService.findByMa(maForm);
        if (Validator.isNotNull(cauHinhEformDTO)) {
            if (Validator.isNotNull(cauHinhEformDTO.getCollection())) {
                response.getWriter().write(cauHinhEformDTO.getCollection());
            }
        }
    }
}
