package vn.sdt.eform.crudform.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.liferay.portal.kernel.upload.UploadPortletRequest;
import com.liferay.portal.kernel.util.PortalUtil;
import com.liferay.portal.kernel.util.Validator;
import lombok.RequiredArgsConstructor;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.portlet.bind.annotation.ResourceMapping;
import vn.sdt.eform.cauhinheform.dto.CauHinhEformDTO;
import vn.sdt.eform.cauhinheform.dto.ResponseDTO;
import vn.sdt.eform.cauhinheform.dto.ServiceDataDTO;
import vn.sdt.eform.cauhinheform.services.CauHinhEformService;
import vn.sdt.eform.crudform.services.DuLieuFormService;
import vn.sdt.eform.utils.Constants;
import vn.sdt.eform.utils.Utils;

import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Controller
@RequestMapping("VIEW")
@RequiredArgsConstructor
public class CRUDFormAjaxController {
    private final DuLieuFormService duLieuFormService;
    private final CauHinhEformService cauHinhEformService;
    @Autowired
    @Qualifier("objectMapper")
    public ObjectMapper objectMapper;

    @ResourceMapping(value = "getDuLieuTable")
    public void getDuLieuTable(ResourceResponse response,
                               @RequestParam(value = "maForm", defaultValue = "") String maForm) throws IOException {

        List<Object> listDuLieu = duLieuFormService.getDanhSachByMaForm(maForm);
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write(objectMapper.writeValueAsString(listDuLieu));
    }

    @ResourceMapping(value = "searchDuLieuForm")
    public void searchDuLieuForm(ResourceResponse response,
                                 @RequestParam(value = "maForm", defaultValue = "") String maForm,
                                 @RequestParam(value = "listTruongSearch", defaultValue = "") String listTruongSearch,
                                 @RequestParam(value = "listValueSearch", defaultValue = "") String listValueSearch) throws IOException {
        List<Object> listDuLieu = duLieuFormService.searchDuLieu(maForm, listTruongSearch, listValueSearch);
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write(objectMapper.writeValueAsString(listDuLieu));
    }

    @ResourceMapping(value = "filter")
    public void search(ResourceRequest request, ResourceResponse response,
                       @RequestParam(value = "maForm", defaultValue = "") String maForm,
                       @RequestParam(value = "offset", defaultValue = "0") int offset,
                       @RequestParam(value = "limit", defaultValue = "10") int limit,
                       @RequestParam(value = "listTruongSearch", defaultValue = "") String listTruongSearch,
                       @RequestParam(value = "listValueSearch", defaultValue = "") String listValueSearch) throws Exception {
        ResponseDTO duLieu = new ResponseDTO();
        CauHinhEformDTO cauHinhEformDTO = cauHinhEformService.findByMa(maForm);
        if (Validator.isNotNull(cauHinhEformDTO)){
            if (cauHinhEformDTO.getLoai() == Constants.LOAI_FORM.DANHSACH_SERVICE){
                if (Validator.isNotNull(cauHinhEformDTO)){
                    ServiceDataDTO serviceDataDTO = cauHinhEformService.findByCauHinhId(cauHinhEformDTO.getId());
                    if (Validator.isNotNull(serviceDataDTO)){
                        duLieu = duLieuFormService.makeAndSendHttpRequest(serviceDataDTO, new JSONObject(),cauHinhEformDTO.getLoai(), offset, limit, listValueSearch);
                    }
                }
            } else {
                duLieu = duLieuFormService.filter(maForm, listTruongSearch, listValueSearch, offset, limit);
            }
        }
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("total", duLieu.getTotalItems());
        jsonObject.put("rows", Utils.objectToJSONArray(duLieu.getData()));
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write(jsonObject.toString());
    }

    @ResourceMapping(value = "searchValidateForm")
    public void searchValidateForm(ResourceResponse response,
                                   @RequestParam(value = "maForm", defaultValue = "") String maForm,
                                   @RequestParam(value = "objectId", defaultValue = "") String objectId,
                                   @RequestParam(value = "keySearch", defaultValue = "") String keySearch,
                                   @RequestParam(value = "valueSearch", defaultValue = "") String valueSearch) throws IOException {

        List<Object> listDuLieu = duLieuFormService.searchDuLieu(maForm, keySearch, valueSearch.trim());
        String data = "false";
        if (listDuLieu != null && listDuLieu.size() > 0) {
            if (Validator.isNotNull(objectId) && listDuLieu.size() == 1){
                if (!new JSONObject(listDuLieu.get(0).toString()).getJSONObject("_id").get("$oid").toString().equals(objectId)){
                    data = "true";
                }
            }else {
                data = "true";
            }
        }

        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write(data);
    }

    @ResourceMapping(value = "getUrlCauHinh")
    public void getUrlCauHinh(ResourceResponse response,
                                   @RequestParam(value = "maForm", defaultValue = "") String maForm,
                              @RequestParam(value = "tenTable", defaultValue = "") String tenTable) throws IOException {
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write(objectMapper.writeValueAsString(duLieuFormService.getUrlCauHinh(maForm, tenTable)));
    }

    @ResourceMapping(value = "loadDanhMucEform")
    public void loadDanhMucEform(ResourceResponse response,
                                 @RequestParam(value = "maForm", defaultValue = "") String maForm,
                                 @RequestParam(value = "valueOption", defaultValue = "") String valueOption,
                                 @RequestParam(value = "labelOption", defaultValue = "") String labelOption,
                                 @RequestParam(value = "filterKey", defaultValue = "") String filterKey,
                                 @RequestParam(value = "filterValue", defaultValue = "") String filterValue) throws Exception {
        CauHinhEformDTO cauHinhEformDTO = cauHinhEformService.findByMa(maForm);
        if (Validator.isNotNull(cauHinhEformDTO)){
            if (cauHinhEformDTO.getLoai() == Constants.LOAI_FORM.DANHSACH_SERVICE){
                Map<Object, Object> map = new HashMap<>();
                if (Validator.isNotNull(cauHinhEformDTO)){
                    ServiceDataDTO serviceDataDTO = cauHinhEformService.findByCauHinhId(cauHinhEformDTO.getId());
                    if (Validator.isNotNull(serviceDataDTO)){
                        ResponseDTO responseDTO = duLieuFormService.makeAndSendHttpRequest(
                                serviceDataDTO, new JSONObject(),cauHinhEformDTO.getLoai(), -1, -1, filterValue);
                        if (Validator.isNotNull(responseDTO)) {
                            JSONArray jsonArray = (JSONArray) responseDTO.getData();
                            jsonArray.forEach(o -> {
                                JSONObject jsonObject = (JSONObject) o;
                                map.put(jsonObject.get(valueOption), jsonObject.get(labelOption));
                            });
                        }
                    }
                }
                response.setContentType("application/json");
                response.setCharacterEncoding("UTF-8");
                response.getWriter().write(objectMapper.writeValueAsString(map));
            } else {
                Map<Object, Object> map = new HashMap<>();
                List<Object> listDuLieu = duLieuFormService.searchDuLieu(maForm, filterKey, filterValue);
                map = listDuLieu.stream().collect(Collectors.toMap(p -> new JSONObject(p.toString()).get(valueOption), p -> new JSONObject(p.toString()).get(labelOption)));
                response.setContentType("application/json");
                response.setCharacterEncoding("UTF-8");
                response.getWriter().write(objectMapper.writeValueAsString(map));
            }
        }
    }

    @ResourceMapping(value = "getDuLieuCauHinh")
    public void getDuLieuCauHinh(ResourceResponse response,
                              @RequestParam(value = "maForm", defaultValue = "") String maForm) throws IOException {
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        CauHinhEformDTO cauHinhEformDTO = cauHinhEformService.findByMa(maForm);
        if (Validator.isNotNull(cauHinhEformDTO)) {
            response.getWriter().write(objectMapper.writeValueAsString(cauHinhEformDTO.getHtml()));
        }
    }

    @ResourceMapping(value = "uploadFile")
    public void uploadFile(ResourceResponse response, ResourceRequest request) throws Exception {

        UploadPortletRequest uploadRequest = PortalUtil.getUploadPortletRequest(request);

        response.getWriter().write(objectMapper.writeValueAsString(duLieuFormService.uploadInfo(uploadRequest, request)));
    }
}
