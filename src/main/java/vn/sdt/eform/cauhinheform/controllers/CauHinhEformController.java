package vn.sdt.eform.cauhinheform.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.servlet.SessionMessages;
import com.liferay.portal.kernel.upload.UploadPortletRequest;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PortalUtil;
import com.liferay.portal.kernel.util.Validator;
import lombok.RequiredArgsConstructor;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;
import vn.sdt.eform.cauhinheform.dto.CauHinhEformDTO;
import vn.sdt.eform.cauhinheform.dto.ResponseDTO;
import vn.sdt.eform.cauhinheform.services.CauHinhEformService;
import vn.sdt.eform.crudform.dto.DuLieuDTO;
import vn.sdt.eform.crudform.services.DuLieuFormService;
import vn.sdt.eform.utils.Constants;
import vn.sdt.eform.utils.Utils;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.RenderRequest;
import java.io.File;
import java.util.List;

@Controller
@RequestMapping("VIEW")
@RequiredArgsConstructor
public class CauHinhEformController {
    private final CauHinhEformService cauHinhEformService;
    private final DuLieuFormService duLieuFormService;
    @Autowired
    @Qualifier("objectMapper")
    public ObjectMapper objectMapper;

    @RenderMapping
    public String view() {
        return "danhsach";
    }

    @RenderMapping(params = "action=themMoi")
    public String themMoi(Model model, RenderRequest request,
                          @RequestParam(value = "layoutId", defaultValue = "col-md-12") String layoutId,
                          @RequestParam(value = "maEform", defaultValue = "") String maEform) {
        maEform = ParamUtil.get(request,"maEform","");
        CauHinhEformDTO cauHinhEformDTO = new CauHinhEformDTO();
        if (Validator.isNotNull(maEform)) {
            cauHinhEformDTO = cauHinhEformService.findByMa(maEform);
            if (Validator.isNotNull(cauHinhEformDTO)){
                model.addAttribute("item", cauHinhEformDTO);
            } else {
                model.addAttribute("item", cauHinhEformDTO);
            }
        } else {
            CauHinhEformDTO cauHinhEformDefault = cauHinhEformService.findByMa(Constants.DEFAULT_FINAL);
            if (cauHinhEformDefault != null) {
                cauHinhEformDTO = cauHinhEformDefault;
            }
            model.addAttribute("item", cauHinhEformDTO);
            model.addAttribute("isAdd", "1");
        }
        List<CauHinhEformDTO> listEformCauHinh = cauHinhEformService.getDanhSach();
        model.addAttribute("listEformCauHinh", listEformCauHinh);
        model.addAttribute("layoutId", layoutId);
        return "themmoi";
    }

    @ActionMapping(params = "action=themMoi")
    public void themMoi(ActionRequest actionRequest, @ModelAttribute("item") CauHinhEformDTO cauHinhEform) {
        cauHinhEformService.saveCauHinhEform(cauHinhEform);
        SessionMessages.add(actionRequest, "success");
    }

    @ActionMapping(params = "action=xoa")
    public void xoa(ActionResponse response, SessionStatus sessionStatus, ActionRequest actionRequest,
                    @RequestParam(value = "maEform", defaultValue = "") String maEform) {
        cauHinhEformService.deleteCauHinhEform(maEform);
        sessionStatus.setComplete();
        SessionMessages.add(actionRequest, "success");
        response.setRenderParameter("action", "");
    }

    @RenderMapping(params = "action=getCauHinhCRUDService")
    public String getCauHinhCRUDService(Model model, @RequestParam(value = "cauHinhId", defaultValue = "") String cauHinhId) {
        CauHinhEformDTO cauHinhEformDTO = new CauHinhEformDTO();
        if (Validator.isNotNull(cauHinhId)) {
            cauHinhEformDTO = cauHinhEformService.findById(cauHinhId);
        }
        model.addAttribute("item", cauHinhEformDTO);
        return "themmoi";
    }

    @ActionMapping(params = "action=importFile")
    public void importFile(ActionRequest actionRequest, ActionResponse actionResponse, SessionStatus sessionStatus, Model model) {
        UploadPortletRequest request = PortalUtil.getUploadPortletRequest(actionRequest);
        File file = request.getFile("fileImport");

        if (Validator.isNotNull(file) && file.length() > 0) {
            ResponseDTO responseDTO = cauHinhEformService.uploadFileEform(file);

            if (responseDTO.getStatus() == Constants.RESPONE.SUCCESS) {
                model.addAttribute("thongBao", "Import thành công");
                SessionMessages.add(actionRequest, "form-success");
                actionResponse.setRenderParameter("maEform", responseDTO.getData().toString());
            } else {
                SessionMessages.add(actionRequest, "form-error");
            }
        }
        actionResponse.setRenderParameter("action", "themMoi");
    }

    @RenderMapping(params = "action=in")
    public String in(Model model, RenderRequest request,
                     @RequestParam(value = "maForm", required = true) String maForm) {
        model.addAttribute("maForm", maForm);

        return "danhsachtemplateprint";
    }

    @RenderMapping(params = "action=themMoiIn")
    public String themMoiIn(Model model, RenderRequest request,
                            @RequestParam(value = "id", required = false) String id,
                            @RequestParam(value = "maForm", defaultValue = "") String maForm) throws Exception {
        CauHinhEformDTO cauHinhEform = new CauHinhEformDTO();
        JSONObject jsonData = null;
        if (Validator.isNotNull(maForm)){
            List<Object> listDuLieu = duLieuFormService.getDanhSachByMaForm(maForm);

            if (listDuLieu.size() > 0){
                List<String> listResult = Utils.getListIdHoSo(new JSONObject(listDuLieu.get(0).toString()));
                if (listResult.size() > 0){
                    model.addAttribute("listIds", listResult);
                }
            }
        }

        if (Validator.isNotNull(id)) {
            jsonData = new JSONObject(duLieuFormService.getDuLieuByMaFormAndObjectId(Constants.MA_FORM_TEMPLATE_PRINT, id).toString());

            if (jsonData != null) {
                cauHinhEform.setId(jsonData.getJSONObject("_id").getString("$oid"));
                if (jsonData.has("maForm")) {
                    cauHinhEform.setMaForm(jsonData.getString("maForm"));
                }
                if (jsonData.has("tenForm")) {
                    cauHinhEform.setTenForm(jsonData.getString("tenForm"));
                }
                if (jsonData.has("html")) {
                    cauHinhEform.setHtml(jsonData.getString("html"));
                }
            }
        }
        model.addAttribute("item", cauHinhEform);

        return "print";
    }

    @ActionMapping(params = "action=themMoiIn")
    public void themMoiIn(ActionRequest actionRequest, @ModelAttribute("item") CauHinhEformDTO cauHinhEform) {
        String collection = Constants.MA_FORM_TEMPLATE_PRINT;
        String maForm = cauHinhEform.getMaForm();
        String html = cauHinhEform.getHtml();
        String tenForm = cauHinhEform.getTenForm();
        JSONObject object = new JSONObject();
        object.put("maForm", maForm);
        object.put("html", html);
        object.put("tenForm", tenForm);

        duLieuFormService.luuDuLieu(collection, object.toString(), cauHinhEform.getId());
        SessionMessages.add(actionRequest, "success");
    }
}
