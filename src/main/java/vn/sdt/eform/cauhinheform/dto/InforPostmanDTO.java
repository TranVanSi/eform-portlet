package vn.sdt.eform.cauhinheform.dto;

import lombok.Data;

@Data
public class InforPostmanDTO {
    private String schema;
    private String name;
    private String _postman_id;
}
