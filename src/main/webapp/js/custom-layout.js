
function openNav() {
    var sideBarWidth = document.getElementById("mySidebar").style.width;
    if (sideBarWidth == "" || sideBarWidth == "0px") {
        document.getElementById("mySidebar").style.width = "250px";
        document.getElementById("main").style.marginRight = "145px";
        document.getElementById("target").style.marginRight = "145px";
    } else {
        document.getElementById("mySidebar").style.width = "0";
        document.getElementById("main").style.marginRight= "0";
        document.getElementById("target").style.marginRight = "0";
    }

}

function closeNav() {
  document.getElementById("mySidebar").style.width = "0";
  document.getElementById("main").style.marginRight= "0";
}
var html1 ;

function replaceAll(str, find, replace) {
    return str.replace(new RegExp(find, 'g'), replace);
}

function openPreview() {
    var copyText = document.getElementById("render");
    copyText.select();
    copyText.setSelectionRange(0, 99999)
    document.execCommand("copy");
    html1 = copyText.value.toString() + "";
    $('.preview').html(html1);
    $('#myModal').addClass('in');

}

function loadLienKetData() {
    var url = $('#urlLoadLienKetData').val();
    $('form.form-horizontal').find('input').each(function () {
        if ($(this).attr("type") == "date"){
            $(this).addClass("form-control sdt-datepicker");
        }
       var tenLienKet = $(this).attr('lienKet');
       if (tenLienKet != '' && typeof tenLienKet != 'undefined'){
           $.ajax({
               type: 'GET'
               , url: url
               , async: false
               , contentType: "application/json; charset=utf-8"
               , data: {tenLienKet: tenLienKet}
               , success: function (data) {
                   $(this).empty();
                   $('[lienKet="' + tenLienKet + '"]').val(data);

               }
           });
       }
    });
}
function loadDanhMucDungChung() {
    var url = $('#urlLoadDanhMuc').val();
    $('form.form-horizontal').find('select').each(function () {

        var danhMuc = $(this).attr("data_danhmuc");

        if (danhMuc != undefined && danhMuc.length > 0 && danhMuc != 'quanHuyen' && danhMuc != 'phuongXa') {
            $.ajax({
                type: 'GET'
                , url: url
                , async: false
                , contentType: "application/json; charset=utf-8"
                , data: {danhMuc: danhMuc}
                , success: function (data) {
                    $(this).empty();
                    var respone = JSON.parse(data);
                    var xhtml = "<option value='0'>---Chọn---</option>";
                    if (typeof respone.objects == 'undefined') {
                        for (var j = 0; j < respone.length; j++) {
                            var objs = respone[j];
                            xhtml += "<option value='" + objs['id'] + "'>";
                            xhtml += objs['ten'];
                            xhtml += "</option>";
                        }
                    } else {
                        for (var i = 0; i < respone.objects.object.length; i++) {
                            var obj = respone.objects.object[i];
                            xhtml += "<option value='" + obj['id'] + "'>";
                            xhtml += obj['ten'];
                            xhtml += "</option>";
                        }
                    }
                    $('[data_danhmuc="' + danhMuc + '"]').html(xhtml);

                }
            });
        }
    });
}

function loadQuanHuyen(id) {
    var url = $('#urlLoadDonViHanhChinh').val();
    var value = $('.preview #'+id).val();
    var dungChung = id.substring(0, id.length-11)+"QuanHuyenId";
    if (value > 0){
        $.ajax({
            type: 'GET'
            , url: url
            , async: false
            , contentType: "application/json; charset=utf-8"
            , data: {chaId: value}
            , success: function (data) {
                $(this).empty();
                var respone = JSON.parse(data);
                var xhtml = "<option value='0'>---Chọn---</option>";
                for (var j = 0; j< respone.length; j++){
                    var objs = respone[j];
                    xhtml += "<option value='" + objs['id'] + "'>";
                    xhtml += objs['ten'];
                    xhtml += "</option>";
                }
                $('.preview #'+dungChung).html(xhtml);

            }
        });
    }

}

function loadPhuongXa(id) {
    var url = $('#urlLoadDonViHanhChinh').val();
    var value = $('.preview #'+id).val();
    var dungChung = id.substring(0, id.length-11)+"PhuongXaId";
    if (value > 0){
        $.ajax({
            type: 'GET'
            , url: url
            , async: false
            , contentType: "application/json; charset=utf-8"
            , data: {chaId: value}
            , success: function (data) {
                $(this).empty();
                var respone = JSON.parse(data);
                var xhtml = "<option value='0'>---Chọn---</option>";
                for (var j = 0; j< respone.length; j++){
                    var objs = respone[j];
                    xhtml += "<option value='" + objs['id'] + "'>";
                    xhtml += objs['ten'];
                    xhtml += "</option>";
                }
                $('.preview #'+dungChung).html(xhtml);

            }
        });
    }

}