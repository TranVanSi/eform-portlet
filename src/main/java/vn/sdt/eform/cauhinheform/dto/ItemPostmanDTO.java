package vn.sdt.eform.cauhinheform.dto;

import com.fasterxml.jackson.databind.JsonNode;
import lombok.Data;

@Data
public class ItemPostmanDTO {
    private Object request;
    private String name;
    private JsonNode response;
}
