package vn.sdt.eform.crudform.client;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;
import vn.sdt.eform.crudform.dto.UploadFileDTO;

import java.io.File;

@Service
@RequiredArgsConstructor
public class FileUploadClient {
    private final String path = "/api/v1/upload";
    @Autowired
    @Qualifier("uploadAPIRest")
    public RestTemplate uploadTemplate;

    public String saveFile2Server(File file) {
        MultiValueMap<String, Object> map = new LinkedMultiValueMap<>();
        map.add("filex", new FileSystemResource(file));
        HttpEntity<MultiValueMap<String, Object>> request = new HttpEntity<>(map);
        UploadFileDTO uploadFileDTO = uploadTemplate.postForObject(path + "/uploadFile", request, UploadFileDTO.class, HttpMethod.POST);
        return uploadFileDTO != null ? uploadFileDTO.getFileDownloadUri() + uploadFileDTO.getId() : "";
    }
}
