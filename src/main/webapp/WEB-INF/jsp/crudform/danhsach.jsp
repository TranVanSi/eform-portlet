<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://java.sun.com/portlet" prefix="portlet" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>
<%@ taglib uri="http://liferay.com/tld/portlet" prefix="liferay-portlet" %>
<%@ include file="../init.jsp" %>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>

<liferay-portlet:renderURL var="editUrl">
    <portlet:param name="action" value="edit"/>
</liferay-portlet:renderURL>
<liferay-portlet:renderURL var="chiTietUrl">
    <portlet:param name="action" value="chiTiet"/>
</liferay-portlet:renderURL>

<liferay-portlet:actionURL var="deleteUrl">
    <portlet:param name="action" value="delete"/>
</liferay-portlet:actionURL>
<liferay-portlet:resourceURL var="urlLoadDanhMucEform" id="loadDanhMucEform" />
<liferay-portlet:resourceURL var="getDuLieuTableUrl" id="getDuLieuTable" />
<liferay-portlet:resourceURL var="searchDuLieuFormUrl" id="searchDuLieuForm" />
<liferay-portlet:resourceURL var="getUrlCauHinh" id="getUrlCauHinh" />
<liferay-portlet:resourceURL var="filterUrl" id="filter">
</liferay-portlet:resourceURL>
<portlet:renderURL var="inURL">
    <portlet:param name="action" value="in"/>
</portlet:renderURL>
<liferay-ui:success key="success" message="Yêu cầu thực hiện thành công!"/>
<c:if test="${not empty jspPage}">
    <script>
        window.location="${jspPage}";
    </script>
</c:if>
<input type="hidden" id="urlLoadDanhMucEform" value="${urlLoadDanhMucEform}" />
<input type="hidden" id="getDuLieuTableUrl" value="${getDuLieuTableUrl}" />
<input type="hidden" id="searchDuLieuFormUrl" value="${searchDuLieuFormUrl}" />
<input type="hidden" id="filterUrl" value="${filterUrl}" />
<input type="hidden" id="getUrlCauHinh" value="${getUrlCauHinh}" />
<input type="hidden" id="listId" value="${cauHinhEformDTO.listId}" />
<input type="hidden" id="maForm" value="${cauHinhEformDTO.maForm}" />
<input type="hidden" id="maFormPage" value="${cauHinhEformDTO.maForm}" />
<input type="hidden" id="saveDuLieuFormUrl" value="${saveDuLieuFormUrl}" />
<input type="hidden" id="inURL" value="${inURL}" />
<input type="hidden" id="tinhThanhSelected" value="${tinhThanhSelected}" />
${cauHinhEformDTO.html}

<form id="chinhSuaForm" method="post" action="${editUrl}" style="display: none">
    <input type="hidden" id="maFormEdit" name="maFormEdit">
    <input type="hidden" id="idEdit" name="idEdit">
    <input type="hidden" id="urlEdit" name="urlEdit">
</form>
<form id="chiTietForm" method="post" action="${chiTietUrl}" style="display: none">
    <input type="hidden" id="maFormChiTiet" name="maFormChiTiet">
    <input type="hidden" id="idChiTiet" name="idChiTiet">
</form>
<form id="deleteForm" method="post" action="${deleteUrl}" style="display: none">
    <input type="hidden" id="maFormDelete" name="maFormDelete">
    <input type="hidden" id="idDelete" name="idDelete">
    <input type="hidden" id="maFormDS" name="maFormDS" value="${cauHinhEformDTO.maForm}">
</form>
<script src="${pageContext.request.contextPath}/js/danhsachform.js"></script>
