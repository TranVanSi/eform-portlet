package vn.sdt.eform.cauhinheform.dto;

import lombok.Data;

import java.util.List;

@Data
public class ResultDTO {
    private List<ItemPostmanDTO> item;
    private InforPostmanDTO info;
}
