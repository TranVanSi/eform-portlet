package vn.sdt.eform.utils;

import freemarker.template.Template;
import freemarker.template.TemplateException;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;
import java.io.IOException;
import java.util.*;

public class EformUtils {

    private static String createTextFromTemplate(Template template, Map<String, Object> model) throws IOException, TemplateException {
        return FreeMarkerTemplateUtils.processTemplateIntoString(template, model);
    }
    public static String createInputCollectionSetting(Template template, String idStr) throws IOException, TemplateException {
        Map<String, Object> model = new HashMap<>();
        model.put("label", idStr);
        model.put("inputId", idStr);

        String html = createTextFromTemplate(template, model);

        return html;
    }

    public static String createInputHtml(Template template, String idStr) throws IOException, TemplateException {
        Map<String, Object> model = new HashMap<>();
        model.put("labelName", idStr);
        model.put("inputId", idStr);

        String html = createTextFromTemplate(template, model);

        return html;
    }

    public static String createThTable(Template template, String nameColum) throws IOException, TemplateException {
        Map<String, Object> model = new HashMap<>();
        model.put("nameColum", nameColum);

        String html = createTextFromTemplate(template, model);

        return html;
    }

    public static String createTable(Template template, String nameDanhSach, String listNameCollums) throws IOException, TemplateException {
        Map<String, Object> model = new HashMap<>();
        model.put("nameDanhSach", nameDanhSach);
        model.put("listNameCollums", listNameCollums);
        String html = createTextFromTemplate(template, model);

        return html;
    }

    public static String createTableCollection(Template template, String listId, String formName) throws IOException, TemplateException {
        Map<String, Object> model = new HashMap<>();
        model.put("listId", listId);
        model.put("formName", formName);
        String html = createTextFromTemplate(template, model);

        return html;
    }

    public static String createCollectionSetting(Template template, String formName, String collectionBody) throws IOException, TemplateException {
        Map<String, Object> model = new HashMap<>();
        model.put("formName", formName);
        model.put("collectionBody", collectionBody);

        String html = createTextFromTemplate(template, model);

        return html;
    }

    public static String createBodyAddForm(Template template, String bodyHtml, String formName) throws IOException, TemplateException {
        Map<String, Object> model = new HashMap<>();
        model.put("formName", formName);
        model.put("bodyHtml", bodyHtml);

        String html = createTextFromTemplate(template, model);

        return html;
    }

    public static JSONObject getBodyData(JSONObject object, JSONObject objData) {
        Set<String> keys = object.keySet();
        List<String> listString = new ArrayList<>();
        List<String> listInt = new ArrayList<>();
        JSONObject result = new JSONObject();
        keys.forEach(key -> {
            Object obj = object.get(key);
            if (obj instanceof Integer) {
                listInt.add(key);
            } else {
                listString.add(key);
            }
        });

        if (listInt.size() > 0) {
            listInt.forEach(item -> {
                Integer value = Integer.parseInt(objData.get(item).toString());
                result.put(item, value);
            });
        }

        if (listString.size() > 0) {
            listString.forEach(item -> {
                result.put(item, objData.getString(item));
            });
        }
        return result;
    }

    public static JSONArray getListDataFromResponseService(JSONObject object) throws Exception {
        try {
            object = object.getJSONObject("objects");
        } catch (Exception e) {
            return null;
        }

        JSONArray objectData = new JSONArray();
        try {
            objectData = object.getJSONArray("object");
        } catch (Exception e) {
            objectData.put(object.getJSONObject("object"));
        }

        return objectData;
    }
}
