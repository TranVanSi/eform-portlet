var getDuLieuCauHinh = $("#getDuLieuCauHinh").val();
var urlUploadFile = $("#urlUploadFile").val();

$("#luuThemMoi").click(function () {
    var isCheck = true;
    var objectId = $("#objectId").val();
    var jsonSelectData = {};
    $('form#formSubmit fieldset div.row').find('select, radio, checkbox, input, textarea').each(function () {
        var name = $(this).attr("required");
        var url = $('#searchValidateFormUrl').val();
        var existing = $(this).attr("update-status");
        var id = $(this).attr("id");
        var value = $(this).val();
        var obj = document.getElementById(id);
        var maForm = $('#maForm').val();
        if (typeof name != 'undefined'){
            if (value == '' || value == 0) {
                obj.setAttribute("style", "background-color:rgb(254,243,244) !important;border: 1px solid red !important;");
                obj.setAttribute("title", "**Vui lòng nhập dữ liệu!");
                isCheck = false;
            } else {
                obj.setAttribute("style", "");
                obj.setAttribute("title", "");
            }
        }

        if (typeof existing != 'undefined' && existing == 'check-existing' && value != ""){
            $.ajax({
                type: 'GET'
                , url: url
                , async: false
                , contentType: "application/json; charset=utf-8"
                , data: {maForm: maForm, keySearch: id, valueSearch: value, objectId: objectId}
                , success: function (data) {
                    if (data == true) {
                        obj.setAttribute("style", "background-color:rgb(254,243,244) !important;border: 1px solid red !important;");
                        obj.setAttribute("title", "**Dữ liệu đã tồn tại!");
                        isCheck = false;
                    } else {
                        obj.setAttribute("style", "");
                        obj.setAttribute("title", "");
                    }
                }
            });
        }

        if ($(this).is("select")) {
            jsonSelectData[id] = $(this).find('option:selected').text();
        }
    });
    $("#dataMapping").val(JSON.stringify(jsonSelectData));

    $('form#formSubmit fieldset div.row').find('table').each(function () {
        var idTable = $(this).attr("id");
        var myTable = document.getElementById("body"+idTable);
        if (typeof idTable != 'undefined') {
            var trs = myTable.getElementsByTagName("tr");
            var key = idTable.substring(0, idTable.length-5);
            var value = [];
            if (typeof trs != 'undefined') {
                for (var i = 0; i < trs.length; i++) {
                    var obj = '';
                    var item = trs[i];
                    var idTr = item.className;
                    obj += $("#"+idTr).attr("value");
                    if (typeof obj != 'undefined'){
                        value.push(obj);
                    }
                }
            }
            var htmls = "<input type='hidden' id='" + key + "' name='" + key + "' value='["+value+"]' />"

            var ids = $("#listId").val();
            if (typeof ids != 'undefined' && ids.indexOf(key) == -1){
                if (ids.length > 0){
                    ids += ","+key;
                }
                $("#listId").val(ids);
                $("#formSubmit").append(htmls);
            }else{
                $("#"+key).val("["+value+"]");
            }

        }

    });

    if (isCheck){
        $("#formSubmit").submit();
    }
});

function uploadFile(id) {
    $("#"+id).click();
    $('#typeUpload').val(id);
    var form = $('#formSubmit')[0];
    var data = new FormData(form);

    $.ajax({
        type: "POST",
        url: urlUploadFile,
        data: data,
        processData: false,
        contentType: false,
        cache: false,
        timeout: 1000000,
        success: function (data, textStatus, jqXHR) {
           var htmls = "";
           var idFile = id+"TaiLen";
           if (typeof data != 'undefined' && data.length > 0){

               htmls += "<input type='hidden' id='" + idFile + "' name='" + idFile + "' value='"+data+"' />"

               var idstr = $("#listId").val();
               if (typeof idstr != 'undefined' && idstr.indexOf(idFile) == -1){
                   if (idstr.length > 0){
                       idstr += ","+idFile;
                   }
                   $("#listId").val(idstr);
                   $("#formSubmit").append(htmls);
               }else{
                   $("#"+idFile).val(data);
               }

           }

        }
        ,
        error: function (jqXHR, textStatus, errorThrown) {
        }

    });
}

function loadPopup(id, loai) {
    console.log("id=:"+id);
    console.log("loai=:"+loai);
    var maForm = $("#"+id).attr("formload");
    var datatarget = $("#"+id).attr("datatarget");
    var table = "";
    $.ajax({
        type: 'GET'
        , url: getDuLieuCauHinh
        , async: false
        , contentType: "application/json; charset=utf-8"
        , data: {
            maForm: maForm
        }
        , success: function (data) {
            if (data !== null) {
                table = data;
            }
        }
    });
    var htmls = '<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#popup'+datatarget+'" id="loadPopup" style="display: none"></button>';
    htmls +='<div class="modal fade" id="popup'+datatarget+'" tabindex="-1" role="dialog" aria-labelledby="successTitle" aria-hidden="true" >\n' +
        '    <div style="width: 100%;height: 50%;min-width: 60%;min-height: 60%;" class="modal-dialog modal-dialog-centered modal-capnhat" role="document">\n' +
        '        <div class="modal-content capNhatTruongThayDoi" style="height: 100%;">\n' +
        '            <div class="modal-header">\n' +
        '                <div style="padding-left: 98%;" type="button" data-dismiss="modal" id="closePopUpSuccess">X</div>\n' +
        '            </div>\n' +
        '            <div class="modal-body row margin0" id="bodyThongBaoSuccess" style="padding: 3%;">'+table+'</div>\n' +
        '<button class="btn btn-primary" onclick="saveRowTableCon(\''+datatarget+'\', \''+loai+'\');" style="margin-left: 45%;width: 100px;margin-bottom: 1%;" >Thêm mới</button>'+
        '        </div>\n' +
        '    </div>\n' +
        '</div>'

    $("#openPopup").html(htmls);
    $(".form-horizontal").prop("style","width: 100%");
    $('#openPopup fieldset div.row').find('select').each(function () {
        var idTR = $(this).attr("id");
        redrawSelectBoxOnLoad(idTR);
    });
    $("#loadPopup").click();
}

function saveRowTableCon(key, loai){
    if (loai != '0'){
        deleteTableCon(loai);
    }
    var idTable = key+"Table";
    var myTable = document.getElementById(idTable);
    var stt = 0;
    var trs = myTable.getElementsByTagName("tr");
    for(var j = 0; j< trs.length; j++){
        var item = trs[j];
        var idTr = item.className;
        if (typeof idTr != 'undefined'){
            var id = idTr.substring(idTable.length);
            if (parseInt(id) > stt){
                stt = parseInt(id);
            }
        }
    }
    stt += 1;
    var idTr = idTable+stt;
    var obj = "{";
    var objmapping = "{";
    var htmls = "";
    $('#bodyThongBaoSuccess fieldset div.row').find('input, select').each(function () {
        if (typeof $(this).attr("name") != 'undefined') {
            var value = $(this).val();
            obj += '"' + $(this).attr("name") + '"' + ' : ' + '"' + value + '"';
            if ($(this).is("select")){
                var text = $(this).find('option:selected').text();
                if (text == "---Chọn---"){
                    htmls += "<td></td>";
                }else{
                    htmls += "<td>"+text+"</td>";
                }

                objmapping += '"' + $(this).attr("name") + '"' + ' : ' + '"' + text + '",';
            }else{
                htmls += "<td>"+value+"</td>"
            }
        }
        obj += ',';
    });
    objmapping = objmapping.substring(0, objmapping.length - 1)+"}";
    obj += '"dataMapping"' + ' : ' + '' + objmapping + '';
    obj += '}';

    $("#closePopUpSuccess").click();
    $(".modal-backdrop").remove();
    htmls += '<td><a class="btn fa fa-pencil-square-o" onclick="editTableCon(\''+idTr+'\',\''+key+'\');"></a><a class="btn fa fa-trash-o" onclick=\"deleteTableCon(\''+idTr+'\');"></a></td>';
    var result = "<tr id='"+idTr+"' class='"+idTr+"' value='"+obj+"'>"+htmls+"</tr>";
    $("#body"+idTable).append(result);
}

var bien;
var bien1;
$(document).ready(function () {
    $(".fixed-table-loading").prop("style","display:none");
    var obj = $("#objResult").text();
    bien = obj;
    loadDanhMucDungChung();
    var isXemChiTiet = $("#isXemChiTiet").val();
   if (typeof obj != 'undefined' && obj != ""){
       var text = $("#txtLegend").text();
       if (isXemChiTiet == "isXemChiTiet"){
           $("#txtLegend").text(text.replace("THÊM MỚI", "CHI TIẾT"));
       }else {
           $("#txtLegend").text(text.replace("THÊM MỚI", "CHỈNH SỬA"));
       }
       $("#objectId").val(JSON.parse(obj)["_id"]["$oid"]);
       $('form#formSubmit fieldset div.row').find('select, radio, checkbox, input, textarea').each(function () {
           var type = $(this).attr("type");
           if (type != "file") {
               var name = $(this).attr("name");
               $("#" + name).val(JSON.parse(obj)[name]);
           }else{
               var name = $(this).attr("name");
               var htmls = "";
               $("#"+name+"Upload").empty();
               var resultUpload = JSON.parse(obj)[name+"TaiLen"];
               if (typeof  resultUpload != 'undefined') {
                   for (var k = 0; k < resultUpload.length; k++) {
                       htmls += "<a href='" + resultUpload[k] + "'>File tải lên</a><br>";
                   }
                   if (htmls.length > 0) {
                       $("#" + name + "Upload").append(htmls);
                       $("#" + name + "Upload").prop("style", "");
                   }
               }
           }
       });
       $('form#formSubmit fieldset div.row').find('table').each(function () {
           var idTable = $(this).attr("id");
           if (typeof idTable != 'undefined') {
               var key = idTable.substring(0, idTable.length-5);
               $("#body" + idTable).empty();
               var data = JSON.parse(obj)[idTable.substring(0, idTable.length-5)];
               bien1 =data;
               if (typeof data != 'undefined') {
                   var result = data;
                   var myTable = document.getElementById(idTable);
                   var ths = myTable.getElementsByTagName("th");

                   for (var j = 0; j < result.length; j ++) {
                       var item = result[j];
                       var dataMap = item['dataMapping'];
                       var keys = Object.keys(item);
                       var stt = j+1;
                       var idTr = idTable+stt;
                       var htmls = "";
                       htmls += "<tr value='"+JSON.stringify(item)+"' id='" + idTr + "' class='" + idTr + "'>";
                       for (var i = 0; i < ths.length; i++) {
                           var name = ths[i].className;
                           var isCheck = true;
                           if (name == 'thaoTac'){
                               isCheck = false;
                           }
                           for (var k = 0; k < keys.length; k++) {
                               var items = keys[k];
                               if (name === items) {
                                   if ( typeof dataMap != "undefined" && dataMap.hasOwnProperty(name)) {
                                       if (dataMap[name] == "---Chọn---"){
                                           htmls += '<td></td>';
                                       }else {
                                           htmls += '<td>' + dataMap[name] + '</td>';
                                       }
                                   } else {
                                       htmls += '<td>' + item[name] + '</td>';
                                   }
                                   isCheck = false;
                               }
                           }
                           if (isCheck){
                               htmls += '<td></td>';
                           }
                       }
                       if (isXemChiTiet != "isXemChiTiet") {
                           htmls += '<td><a class="btn fa fa-pencil-square-o" onclick="editTableCon(\'' + idTable + stt + '\',\'' + key + '\');"></a><a class="btn fa fa-trash-o" onclick="deleteTableCon(\'' + idTable + stt + '\');"></a></td>';
                       }
                       htmls += "</tr>";
                       $("#body" + idTable).append(htmls);

                   }
               }
           }
       });
   }else {
       $('form#formSubmit fieldset div.row').find('table').each(function () {
           var tableId = $(this).attr("id");
           if (typeof tableId != 'undefined') {
               $("#body" + tableId).empty();
           }
       });
   }
    if (isXemChiTiet == "isXemChiTiet") {
        $('form#formSubmit fieldset div.row').find('select, input, textarea').each(function () {
            $(this).prop('disabled', true);
        });
    }
})

function editTableCon(idTrTable, key) {
    var duLieu = $("#"+idTrTable).attr("value");

    loadPopup('btn'+key, idTrTable);

    $('#'+key+' fieldset div.row').find('input, select').each(function () {
        var id = $(this).attr("id");
        $("#"+id).val(JSON.parse(duLieu)[id]);
    });


}


function deleteTableCon(idTrTable) {
    $("#"+idTrTable).remove();
}


function loadDanhMucDungChung() {
    $('form#formSubmit fieldset div.row').find('select').each(function () {
        var elementId = $(this).attr("id");
        redrawSelectBoxOnLoad(elementId);
    });
}

function validateImport() {
    var listId = $('#listId').val().split(",");
    var listKeyId = "";
    var listKeyMergeId = "";
    for (var val of listId) {
        var updateStatus = $('#' + val).attr('update-status');
        if (typeof updateStatus == 'check-existing') {
            listKeyId = listKeyId + "," + val;
        } else if (typeof updateStatus == 'merge') {
            listKeyMergeId = listKeyMergeId + "," + val;
        }
    }
    $('#listKeyId').val(listKeyId);
    $('#listKeyMergeId').val(listKeyMergeId);
    if ($('#fileImport').val().length > 0) {
        $('.btn-import').prop('disabled','');
    }
}

function redrawSelectBoxOnLoad(elementId) {
    var url = $('#urlLoadDanhMucEform').val();
    var eform = $("#" + elementId).attr("eform");
    var valueOption = $("#" + elementId).attr("key-eform");
    var labelOption = $("#" + elementId).attr("label-eform");
    var filterKey = $("#" + elementId).attr("filter_key_eform");
    var curValue = "0";
    if (bien != null && bien.length > 0) {
        curValue = JSON.parse(bien)[elementId];
    }

    if (typeof filterKey != "undefined" && filterKey == 0) {
        if (typeof eform != undefined && eform.length > 0) {
            $.ajax({
                type: 'GET'
                , url: url
                , async: false
                , contentType: "application/json; charset=utf-8"
                , data: {
                    maForm: eform,
                    valueOption: valueOption,
                    labelOption: labelOption
                }
                , success: function (data) {
                    if (data !== null) {
                        $(this).empty();
                        var xhtml = "<option value='0'>---Chọn---</option>";
                        $.map(JSON.parse(JSON.stringify(data)), function (value, key) {
                            xhtml += "<option value='" + key + "'>" + value + "</option>";
                        });
                        $("#" + elementId).html(xhtml);
                    }
                }
            });
            $("#" + elementId).val(curValue).change();
        }
    }
}

function redrawSelectBox(elementId, queryParam) {
    var url = $('#urlLoadDanhMucEform').val();
    var eform = $("#" + elementId).attr("eform");
    var valueOption = $("#" + elementId).attr("key-eform");
    var labelOption = $("#" + elementId).attr("label-eform");
    var filterKey = $("#" + elementId).attr("filter_key_eform");
    var curValue = "";
    if (bien != null && bien.length > 0) {
        curValue = JSON.parse(bien)[elementId];
    }

    if (typeof eform != undefined && eform.length > 0) {
        $.ajax({
            type: 'GET'
            , url: url
            , async: false
            , contentType: "application/json; charset=utf-8"
            , data: {
                maForm: eform,
                valueOption: valueOption,
                labelOption: labelOption,
                filterKey: filterKey,
                filterValue: queryParam
            }
            , success: function (data) {
                if (data !== null) {
                    $(this).empty();
                    var xhtml = "<option value='0'>---Chọn---</option>";
                    $.map(JSON.parse(JSON.stringify(data)), function (value, key) {
                        xhtml += "<option value='" + key + "'>" + value + "</option>";
                    });
                    $("#" + elementId).html(xhtml);
                }
            }
        });
        $("#" + elementId).val(curValue).change();
    }
}