package vn.sdt.eform.cauhinheform.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ServiceDataDTO {
    private String id;
    private String cauHinhId;
    private String dataJson;
    private String token;
}
