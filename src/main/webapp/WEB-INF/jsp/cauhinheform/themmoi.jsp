<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<%@ include file="../init.jsp" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<liferay-portlet:actionURL var="themMoiURL">
    <liferay-portlet:param name="action" value="themMoi"/>
    <liferay-portlet:param name="maCRUDSer" value="${maFormCRUD}"/>
</liferay-portlet:actionURL>


<liferay-portlet:actionURL var="importFileURL">
    <liferay-portlet:param name="action" value="importFile"/>
</liferay-portlet:actionURL>

<liferay-portlet:renderURL var="homeUrl">
    <portlet:param name="action" value=""/>
</liferay-portlet:renderURL>
<liferay-portlet:renderURL var="getCauHinhCRUDServiceUrl">
    <portlet:param name="action" value="getCauHinhCRUDService"/>
</liferay-portlet:renderURL>
<liferay-portlet:resourceURL var="urlLoadDanhMuc" id="loadDanhMucDungChung" />
<liferay-portlet:resourceURL var="urlLoadLienKetData" id="loadLienKetData" />
<liferay-portlet:resourceURL var="urlLoadDonViHanhChinh" id="loadDonViHanhChinhByChaId" />
<liferay-portlet:resourceURL var="getDanhSachCauHinhUrl" id="getDanhSachCauHinh" />
<liferay-portlet:resourceURL var="getListIdCauHinhUrl" id="getListIdCauHinh" />
<liferay-portlet:resourceURL var="getTemplateMauUrl" id="getTemplateMau" />
<liferay-portlet:resourceURL var="urlCheckInfoUpload" id="getInfoUpload" />
<liferay-portlet:resourceURL var="checkValidateDuLieuUrl" id="checkValidateDuLieu" />
<liferay-portlet:resourceURL var="removeDuLieuUrl" id="removeDuLieu" />
<liferay-portlet:resourceURL var="getDanhSachEformUrl" id="getDanhSachCauHinhByLoai" />
<liferay-portlet:resourceURL var="getDanhSachServiceUrl" id="getDanhSachService" />
<html lang="en">
<meta http-equiv="content-type" content="text/html;charset=UTF-8"/>
<head>
    <meta charset="utf-8">
    <title>Eform</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/css/themmoicauhinh.css">
</head>
<div id="loading" style="display:none">
    <img src="${pageContext.request.contextPath}/images/loading.gif" alt="Loading..."/>
</div>
<body>
<div id="busybox" class="bg">
    <div></div>
</div>
<main class="main-table-custom main-themmoigiayto">
    <input type="hidden" id="getTemplateMauUrl" value="${getTemplateMauUrl}" />
    <input type="hidden" id="homeUrl" value="${homeUrl}" />
    <input type="hidden" id="urlLoadLienKetData" value="${urlLoadLienKetData}" />
    <input type="hidden" id="urlLoadDonViHanhChinh" value="${urlLoadDonViHanhChinh}" />
    <input type="hidden" id="getDanhSachCauHinhUrl" value="${getDanhSachCauHinhUrl}" />
    <input type="hidden" id="getListIdCauHinhUrl" value="${getListIdCauHinhUrl}" />
    <input type="hidden" id="urlCheckInfoUpload" value="${urlCheckInfoUpload}" />
    <input type="hidden" id="checkValidateDuLieuUrl" value="${checkValidateDuLieuUrl}" />
    <input type="hidden" id="removeDuLieuUrl" value="${removeDuLieuUrl}" />
    <input type="hidden" id="getDanhSachEformUrl" value="${getDanhSachEformUrl}" />
    <input type="hidden" id="getDanhSachServiceUrl" value="${getDanhSachServiceUrl}" />
    <div class="col-md-12 col-sm-12 col-xs-12 center">
        <label class="tieuDe">THÊM MỚI CẤU HÌNH EFORM</label>
    </div>
        <div class="container cauHinhEform text-left" id="cauHinhEform" enctype="multipart/form-data">
            <div id="main" class="row">
                <div class="col-md-4 col-sm-4 col-xs-12">
                    <label class="text" for="maEform">Mã tiện ích<span class="red">*</span></label>
                    <input name="maEform" id="maEform" class="form-control mb-2" value="${item.maForm}"/>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-12">
                    <label class="text" for="tenEform">Tên tiện ích<span class="red">*</span></label>
                    <input name="tenEform" id="tenEform" class="form-control mb-2" value="${item.tenForm}"/>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-12">
                    <label class="text" for="loaiForm">Loại cấu hình<span class="red">*</span></label>
                    <select id="loaiForm" name="loaiForm" class="form-control" onchange="getUpload(this.value);">
                        <option value="1" ${item.loai == 1 ? 'selected' : ''}>Thêm mới</option>
                        <option value="2" ${item.loai == 2 ? 'selected' : ''}>Danh sách</option>
                        <option value="3" ${item.loai == 3 ? 'selected' : ''}>Chi tiết</option>
                        <option value="4" ${item.loai == 4 ? 'selected' : ''}>CREATE SERVICE</option>
                        <option value="5" ${item.loai == 5 ? 'selected' : ''}>DANH SACH SERVICE</option>
                        <option value="6" ${item.loai == 6 ? 'selected' : ''}>Import file</option>
                    </select>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" id="anHienUpLoad" style="display:none">
                    <label class="text" for="loaiForm">Upload file postman json</label>
                    <input type="file" id="fileTaiLen" hidden name="fileTaiLen"/>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 taiFile giayToTaiLen">
                        <label class="uploadCustom" for="fileTaiLen">Chọn file</label>
                        <label id="file-chosen"> Chưa chọn file</label>
                    </div>
                </div>

                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" id="import_file" style="display: none">
                    <form method="post" name="formImport" id="formImport" action="${importFileURL }"
                          enctype='multipart/form-data' class="needs-validation" novalidate="true">
                        <div class="file-button">
                            <label class="w-100">Import file</label>
                            <input class="w-50" name="fileImport" id="fileImport" type="file"
                                   accept=".xls,.xlsx" title="Import file" onchange="enableBtn();">
                            <button type="submit" class="btn btn-primary btn-import" disabled>Tạo EForm</button>
                        </div>
                    </form>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12 paddingbt1">
                    <label class="text">Chọn template mẫu</label>
                    <select id="chonTemplate" name="chonTemplate" class="form-control" onchange="getTemplateMau(this.value);">
                        <option value="0">---Chọn---</option>
                        <c:forEach items="${listEformCauHinh}" var="eform">
                            <option value="${eform.maForm}">${eform.tenForm}</option>
                        </c:forEach>
                    </select>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <label class="text">Trạng thái tiện ích<span class="red">*</span></label>
                    <div class="d-flex align-items-center">
                        <input type="radio" id="chiaSe" name="trangThaiEform" value="true"
                               <c:if test="${item.trangThaiForm eq true}">checked="true"</c:if>>
                        <label for="chiaSe" class="mb-0 mr-5 ml-2" style="color: black">Hoạt động</label><br>
                        <input type="radio" id="khongChiaSe" name="trangThaiEform" value="false"
                               <c:if test="${item.trangThaiForm eq false}">checked="false"</c:if>>
                        <label for="khongChiaSe" class="mb-0 ml-2" style="color: black">Khóa</label><br>
                    </div>
                </div>
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <button class="btn btn-primary success" onclick="openNav()">☰ Các thành phần html</button>
                    <button class="openbtn d-none" onclick="openPreview()">Xem trước</button>
                    <button type="button" class="btn btn-primary success" data-toggle="modal" data-target="#myModal" onclick="openPreview()">
                        Xem trước
                    </button>
                    <input type="hidden" name="collectionEdit" value="${item.collection}" id="collectionEdit"/>
                </div>
            </div>

            <div class="row clearfix">
                <div id="mySidebar" class="sidebar" style="background: #0156A7;">
                    <div class="tabbable">
                        <ul class="nav nav-tabs" id="formtabs">
                        </ul>
                        <form class="form-horizontal" enctype="multipart/form-data" id="components" role="form">
                            <fieldset>
                                <div class="tab-content">
                                </div>
                            </fieldset>
                        </form>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="clearfix">
                        <div id="build">
                            <form id="target" class="form-horizontal" enctype="multipart/form-data">
                            </form>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-12 text-center paddingbottom1 paddingtop1">
                <button type="button" id="luueform" class="btn btn-primary success" onclick="luuEform();">Lưu cấu hình</button>
                <button type="button" class="btn btn-primary success" id="btnQuayLai" >Quay lại</button>
            </div>
        </div>
</main>

<div style="display: none">
    <form:form action="${themMoiURL}" method="post" id="formLuuEform" modelAttribute="item">
        <form:input type="hidden" path="html" id="html"/>
        <form:input type="hidden" path="tenForm" />
        <form:input type="hidden" path="collection" id="collection"/>
        <form:input type="hidden" path="maForm" />
        <form:input type="hidden" path="trangThaiForm"/>
        <form:input type="hidden" path="listId"/>
        <form:input type="hidden" path="id"/>
        <form:input type="hidden" path="loai"/>
    </form:form>
    <input type="hidden" id="isAdd" value="${isAdd}"/>
</div>
<input type="hidden" name="urlLoadDanhMuc" id="urlLoadDanhMuc" value="${urlLoadDanhMuc}"/>
<div class="modal fade" id="myModal" role="dialog" style="display: none">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Nội dung định nghĩa</h4>
            </div>
            <div class="modal-body preview">
                <p>Some text in the modal.</p>
            </div>
            <div class="modal-footer">
                <button type="button" style="margin-right: 46%;" class="btn btn-primary success" data-dismiss="modal">Đóng</button>
            </div>
        </div>

    </div>
</div>
<form action="${getCauHinhCRUDServiceUrl}" method="post" id="formGetHtml" style="display:none;">
    <input type="hidden" name="cauHinhId" id="cauHinhId">
</form>
<input type="hidden" id="loadJS" value="${pageContext.request.contextPath}/js/main-built_backup_7.js" />
<script src="${pageContext.request.contextPath}/js/custom-layout.js"></script>
<script src="${pageContext.request.contextPath}/js/themmoicauhinh.js"></script>
</body>
</html>
